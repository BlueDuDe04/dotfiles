{
  description = "My Flake Dotfiles";

  outputs = _: import ./. _;

  inputs = {
    ### Deps ###
    flake-utils.url = "github:numtide/flake-utils";
    ### Deps ###

    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11-small";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixgl = {
      url = "github:guibou/nixGL";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    impermanence.url = "github:nix-community/impermanence";

    stylix = {
      url = "github:danth/stylix/release-24.11";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        home-manager.follows = "home-manager";
        flake-utils.follows = "flake-utils";
      };
    };

    nix-topology = {
      url = "github:oddlama/nix-topology";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    nix-comfyui = {
      url = "github:dyscorv/nix-comfyui";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    monado-controller-tracking = {
      url = "git+https://gitlab.freedesktop.org/thaytan/monado?ref=dev-constellation-controller-tracking";
      flake = false;
    };

    basalt-monado = {
      url = "git+https://gitlab.freedesktop.org/mateosss/basalt?submodules=1";
      flake = false;
    };

    iptvnator = {
      url = "https://github.com/4gray/iptvnator/releases/download/v0.15.1/IPTVnator-0.15.1.AppImage";
      flake = false;
    };

    gameyfin = {
      url = "https://github.com/gameyfin/gameyfin/releases/download/v1.4.6/gameyfin-1.4.6.jar";
      flake = false;
    };

    suyu = {
      url = "https://git.suyu.dev/suyu/suyu/releases/download/v0.0.3/Suyu-Linux_x86_64.AppImage";
      flake = false;
    };

    plex-htpc = {
      url = "https://api.snapcraft.io/api/v1/snaps/download/81OP06hEXlwmMrpMAhe5hyLy5bQ9q6Kz_64.snap";
      flake = false;
    };

    which-key-nvim = {
      url = "github:folke/which-key.nvim?ref=v2.1.0";
      flake = false;
    };

    trouble-nvim = {
      url = "github:folke/trouble.nvim?ref=v2.10.0";
      flake = false;
    };

    disassemble-nvim = {
      url = "github:mdedonno1337/disassemble.nvim";
      flake = false;
    };
  };
}
