{
  ...
}:

{
  pkgs,
  ...
}:

{
  xdg.configFile = {
    "starship.toml".source = ./starship.toml;
    "lf/icons".source = ./lf/icons;
    "fish/functions/lfcd.fish".source = ./lf/lfcd.fish;
  };

  home = {
    stateVersion = "22.11"; # Please read the comment before changing.

    sessionVariables = {
      DIRENV_LOG_FORMAT = "";
      email = "Bennettmason04@gmail.com";
    };
  };

  # Let Home Manager install and manage itself.
  programs = {
    home-manager.enable = true;

    git = {
      enable = true;
      lfs.enable = true;
      userName = "BlueDuDe04";
      userEmail = "Bennettmason04@gmail.com";

      extraConfig = {
        url = {
          "git@github.com:" = {
            insteadOf = "https://github.com/";
          };
        };
      };
    };

    lf = {
      enable = true;
      
      package = pkgs.writeShellScriptBin "lf" ''${pkgs.lf}/bin/lf "$@"'';

      settings = {
        preview = true;
        hidden = true;
        drawbox = true;
        icons = true;
        ignorecase = true;
      };

      # commands = {
      # };

      keybindings = {
        "<esc>" = '':quit'';
      };

      extraConfig = 
      let 
        previewer = 
          pkgs.writeShellScriptBin "pv.sh" ''
          file=$1
          w=$2
          h=$3
          x=$4
          y=$5
          
          if [[ "$( ${pkgs.file}/bin/file -Lb --mime-type "$file")" =~ ^image ]]; then
              ${pkgs.kitty}/bin/kitty +kitten icat --silent --stdin no --transfer-mode file --place "''${w}x''${h}@''${x}x''${y}" "$file" < /dev/null > /dev/tty
              exit 1
          fi
          
          ${pkgs.pistol}/bin/pistol "$file"
        '';

        cleaner = pkgs.writeShellScriptBin "clean.sh" ''
          ${pkgs.kitty}/bin/kitty +kitten icat --clear --stdin no --silent --transfer-mode file < /dev/null > /dev/tty
        '';
      in ''
        set cleaner ${cleaner}/bin/clean.sh
        set previewer ${previewer}/bin/pv.sh
      '';
    };

    fish = {
      enable = true;

      package = pkgs.stdenv.mkDerivation {
        name = "fish";
        src = pkgs.fish;
        installPhase = ''
          mkdir -p $out/share
          for item in $src/*; do
              if [ ! "$item" == "$src/share" ]; then
                  ln -s $src/$(basename $item) $out/$(basename $item)
              fi
          done
          for item in $src/share/*; do
              if [ ! "$item" == "$src/share/applications" ]; then
                  ln -s $src/share/$(basename $item) $out/share/$(basename $item)
              fi
          done
        '';
      };

      shellInit = ''
        set fish_greeting

        ${pkgs.nix-your-shell}/bin/nix-your-shell fish | source

        bind \co 'set old_tty (stty -g); stty sane; lfcd; stty $old_tty; commandline -f repaint'

        # name: 'Catppuccin mocha'
        # url: 'https://github.com/catppuccin/fish'
        # preferred_background: 1e1e2e

        set -g fish_color_normal cdd6f4
        set -g fish_color_command 89b4fa
        set -g fish_color_param f2cdcd
        set -g fish_color_keyword f38ba8
        set -g fish_color_quote a6e3a1
        set -g fish_color_redirection f5c2e7
        set -g fish_color_end fab387
        set -g fish_color_comment 7f849c
        set -g fish_color_error f38ba8
        set -g fish_color_gray 6c7086
        set -g fish_color_selection --background=313244
        set -g fish_color_search_match --background=313244
        set -g fish_color_option a6e3a1
        set -g fish_color_operator f5c2e7
        set -g fish_color_escape eba0ac
        set -g fish_color_autosuggestion 6c7086
        set -g fish_color_cancel f38ba8
        set -g fish_color_cwd f9e2af
        set -g fish_color_user 94e2d5
        set -g fish_color_host 89b4fa
        set -g fish_color_host_remote a6e3a1
        set -g fish_color_status f38ba8
        set -g fish_pager_color_progress 6c7086
        set -g fish_pager_color_prefix f5c2e7
        set -g fish_pager_color_completion cdd6f4
        set -g fish_pager_color_description 6c7086
      '';
    };

    starship = {
      enable = true;
      enableFishIntegration = true;
    };

    zoxide = {
      enable = true;
      enableFishIntegration = true;
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
      # enableFishIntegration = true;
    };
  };
}
