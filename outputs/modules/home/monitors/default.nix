{
  ...
}:

{
  lib,
  config,
  pkgs,
  ...
}:

let
  inherit (builtins) map concatLists filter concatStringsSep;
  inherit (lib) mkOption types;

  cfg = config.monitors;
in

{
  options.monitors = mkOption {
    type = types.listOf (types.submodule {
      options = {
        name = mkOption {
          type = types.str;
          example = "DP-1";
        };
        primary = mkOption {
          type = types.bool;
          default = false;
        };
        wallpaper = mkOption {
          type = types.nullOr (types.submodule {
            options = {
              img = mkOption {
                type = types.path;
              };
              mode = mkOption {
                type = types.nullOr types.str;
                default = null;
              };
            };
          });
          default = null;
        };
        width = mkOption {
          type = types.int;
          example = 1920;
        };
        height = mkOption {
          type = types.int;
          example = 1080;
        };
        refreshRate = mkOption {
          type = types.int;
          default = 60;
        };
        x = mkOption {
          type = types.int;
          default = 0;
        };
        y = mkOption {
          type = types.int;
          default = 0;
        };
        scale = mkOption {
          type = types.str;
          default = "1";
        };
        transform = mkOption {
          type = types.nullOr types.str;
          default = null;
        };
        enabled = mkOption {
          type = types.bool;
          default = true;
        };
        workspaces = mkOption {
          type = types.listOf types.str;
          default = [];
        };
      };
    });
    default = [];
  };

  config = {
    wayland.windowManager.hyprland.settings = let
      hyprSetMonitor = m: let
        resolution = "${toString m.width}x${toString m.height}@${toString m.refreshRate}";
        position = "${toString m.x}x${toString m.y}";
        transform = if m.transform != null then "transform,${m.transform}" else "";
      in
        "${m.name},${if m.enabled then "${resolution},${position},${m.scale},${transform}" else "disable"}";

      swaybgSetWallpaper = m: let
        monitor = " -o ${m.name} ";
        mode = if m.wallpaper.mode != null then " -m ${m.wallpaper.mode} " else "";
        img = " -i ${m.wallpaper.img} ";
      in
        if m.wallpaper != null then "${monitor}${mode}${img}" else "";

      hyprSetWorkspaces = m: map (w: "${m.name}, ${w}") m.workspaces;
    in {
      monitor = map hyprSetMonitor cfg;
      exec = [
        "pkill swaybg ; ${pkgs.swaybg}/bin/swaybg ${concatStringsSep " " (map swaybgSetWallpaper cfg)}"
      ];
      workspace = concatLists (map hyprSetWorkspaces cfg);
    };
  };
}
