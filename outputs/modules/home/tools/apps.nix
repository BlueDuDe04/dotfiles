{
  pkgs,
  ...
}:

with pkgs; [
  vesktop
  pavucontrol
  obs-studio
  mpv

  blender
  krita

  gnome-calculator
  nautilus
]
