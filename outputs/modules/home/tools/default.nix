{
  ...
}:

{
  pkgs,
  lib,
  config,
  ...
}:

let
  inherit (lib) types mkOption mkIf;

  cfg = config.tools;
  
  cmdline = if cfg.cmdline then (import ./cmdline.nix) pkgs else [];
  apps = if cfg.cmdline then (import ./apps.nix) pkgs else [];
  wayland = if cfg.cmdline then (import ./wayland.nix) pkgs else [];
in

{
  options.tools = mkOption {
    type = types.submodule {
      options = {
        enable = mkOption {
          type = types.bool;
          default = false;
        };

        cmdline = mkOption {
          type = types.bool;
          default = false;
        };

        apps = mkOption {
          type = types.bool;
          default = false;
        };

        wayland = mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
    default = {};
  };

  config = mkIf cfg.enable {
    home.packages = cmdline ++ apps ++ wayland;
  };
}
