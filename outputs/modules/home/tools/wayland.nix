{
  pkgs,
  ...
}:

with pkgs; [
  eww
  fuzzel

  swaybg

  hyprpicker

  grim
  slurp
  swappy
  wf-recorder
  wtype

  wl-clipboard
  cliphist
]
