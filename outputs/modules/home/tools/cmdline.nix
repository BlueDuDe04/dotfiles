{
  pkgs,
  ...
}:

with pkgs; [
  # CLI
  git
  wget
  zip
  unzip
  openvpn
  evtest
  pamixer
  brightnessctl
  imagemagick
  gojq
  ripgrep
  fd
  clipboard-jh

  # TUI
  fzf
  htop
  lazygit
]
