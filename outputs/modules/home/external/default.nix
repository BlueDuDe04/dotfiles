{
  inputs,
  ...
}:

with inputs; {
  sops-nix = sops-nix.homeManagerModules.sops;
  impermanence = impermanence.nixosModules.home-manager.impermanence;
  stylix = stylix.homeManagerModules.stylix;
}
