{
  ...
}:

{
  inputs,
  system,
  pkgs,
  lib,
  config,
  ...
}:

let
  inherit (lib) types mkOption mkIf;

  cfg = config.myFirefox;

  ctrl_number_to_switch_tabs = inputs.firefox-addons.lib.${system}.buildFirefoxXpiAddon rec {
    pname = "ctrl_number_to_switch_tabs";
    version = "1.0.2";
    addonId = "{84601290-bec9-494a-b11c-1baa897a9683}";
    url = "https://addons.mozilla.org/firefox/downloads/file/4192880/${pname}-${version}.xpi";
    sha256 = "sha256-d30B3e2vAnQ29GUYFM/pRlhVLuMRvux0V1NjNAFVCmg=";
    meta = with lib; {
      homepage = "https://github.com/AbigailBuccaneer/firefox-ctrlnumber";
      description = "Adds keyboard shortcut Ctrl+1 to switch to the first tab, Ctrl+2 to switch to the second, and so on. Ctrl+9 switches to the last tab.";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
in

{
  options.myFirefox = mkOption {
    type = types.submodule {
      options = {
        enable = mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
    default = {};
  };

  config = mkIf cfg.enable {
    programs.firefox = {
      enable = true;

      profiles.mason = {
        extensions = with inputs.firefox-addons.packages.${system}; [
          ublock-origin 
          sponsorblock
          darkreader
          bitwarden
          ctrl_number_to_switch_tabs
        ];

        userChrome = builtins.readFile ./userChrome.css;

        settings = {
          # Theme
          "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

          # New Tab
          "browser.startup.homepage" = "chrome://browser/content/blanktab.html";
          "browser.newtabpage.enabled" = false;

          # Options
          "browser.fullscreen.autohide" = false;
          "browser.toolbars.bookmarks.visibility" = "never";
          "signon.rememberSignons" = false;
          "browser.download.useDownloadDir" = false;
          "browser.aboutConfig.showWarning" = false;

          # XDG-Desktop-Portal
          "widget.use-xdg-desktop-portal.file-picker" = 1;
          "widget.use-xdg-desktop-portal.mime-handler" = 1;
          "widget.use-xdg-desktop-portal.settings" = 1;
          "widget.use-xdg-desktop-portal.location" = 1;
          "widget.use-xdg-desktop-portal.open-uri" = 1;

          # Security
          "dom.security.https_only_mode" = true;

          # Video Hardware Acceleration
          "media.ffmpeg.vaapi.enabled" = true;
        };

        search = {
          default = "DuckDuckGo";

          force = true;

          order = [
            "DuckDuckGo"
            "Nix Packages"
            "NixOs Wiki"
          ];

          engines = {
            "Nix Packages" = {
              urls = [{
                template = "https://search.nixos.org/packages";
                params = [
                  { name = "type"; value = "packages"; }
                  { name = "query"; value = "{searchTerms}"; }
                ];
              }];

              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = [ "@np" ];
            };

            "NixOS Wiki" = {
              urls = [{ template = "https://nixos.wiki/index.php?search={searchTerms}"; }];
              iconUpdateURL = "https://nixos.wiki/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000; # every day
              definedAliases = [ "@nw" ];
            };
          };
        };
      };
    };
  };
}
