{
  ...
}:

{
  ...
}:

{
  systemd.user.services.drata-agent = {
    Unit = {
      Description = "Run Drata Agent on login.";
      After = [ "graphical-session.target" ];
    };

    Install = {
      WantedBy = [ "graphical-session.target" ];
    };

    Service = {
      ExecStart = "drata-agent";
    };
  };
}
