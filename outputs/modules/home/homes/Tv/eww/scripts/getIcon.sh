#!/usr/bin/env bash

if [[ ! $1 ]]; then
    echo "./images/notFound.png"
    exit 1;
fi

if [[ ! $2 ]]; then
    echo "./images/notFound.png"
    exit 1;
fi

desktop_file=/etc/profiles/per-user/${USER}/share/applications/"$1".desktop

if [[ ! -e $desktop_file ]]; then
    desktop_file=$(grep -l "Name=$1" /etc/profiles/per-user/${USER}/share/applications/*.desktop | head -n 1);
fi

if [[ ! -e $desktop_file ]]; then
    desktop_file=$(grep -l "Name=$2" /etc/profiles/per-user/${USER}/share/applications/*.desktop | head -n 1);
fi

if [[ ! -e $desktop_file ]]; then
    desktop_file=/etc/profiles/per-user/${USER}/share/applications/"$2".desktop
fi

if [[ ! -e $desktop_file ]]; then
    echo "./images/notFound.png"
    exit 1;
fi

icon_name=$(grep -o 'Icon=.*' $desktop_file | cut -d '=' -f 2)

# Is Path
if [ -e "$icon_name" ]; then
    echo $icon_name
else
    res=$(ls /etc/profiles/per-user/${USER}/share/icons/hicolor/*/apps/* | grep $icon_name | grep -oE '[0-9]+' | sort -n | tail -1)

    img=/etc/profiles/per-user/${USER}/share/icons/hicolor/scalable/apps/${icon_name}.*

    if [[ $(echo $img | grep "*") == "" ]]; then
        echo $img;
    else
        img=/etc/profiles/per-user/${USER}/share/icons/hicolor/${res}x${res}/apps/${icon_name}.*
        echo $img
    fi
fi
