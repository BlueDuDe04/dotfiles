#!/usr/bin/env bash

calculate_seconds() {
    duration="$1"
    IFS=':' read -r -a parts <<< "$duration"
    len="${#parts[@]}"
    case "$len" in
        1)  # Only seconds provided
            total_seconds="${parts[0]}"
            ;;
        2)  # Minutes and seconds provided
            total_seconds=$((parts[0] * 60 + parts[1]))
            ;;
        3)  # Hours, minutes, and seconds provided
            total_seconds=$((parts[0] * 3600 + parts[1] * 60 + parts[2]))
            ;;
        *)  # Invalid format
            echo "Invalid duration format: $duration"
            exit 1
            ;;
    esac
    echo "$total_seconds"
}

check_bedtime() {
    bedtime="$1"
    current_time=$(date +%H:%M)
    if [ "$current_time" \> "$bedtime" ]; then
        echo "It's past bedtime!"
    else
        echo "It's not past bedtime."
    fi
}


# Function to monitor player status
monitor_player() {
    local player="$1"
    playerctl -p "$player" status --format "{{ uc(status) }}" --follow |
    while read -r event; do
        case "$event" in
            PLAYING)
                echo "A video is currently playing in $player."
                # Get the duration of the video using playerctl
                duration=$(playerctl metadata -p "$player" -f '{{ duration(mpris:length - position) }}')

                # Check if duration is not null
                if [ -n "$duration" ]; then
                    # Convert the duration to seconds
                    duration_seconds=$(calculate_seconds "$duration")

                    # Calculate the finish time using the obtained duration
                    finish_time=$(( $(date +%s) + duration_seconds ))

                    echo "Video in $player will finish at: $(date -d @"$finish_time" +'%H:%M')"
                    check_bedtime "$(date -d @"$finish_time" +'%H:%M')"
                else
                    echo "Could not retrieve duration for the video in $player."
                    check_bedtime "$(date +%H:%M)"
                fi
                ;;
            PAUSED)
                echo "Video playback is paused in $player."
                ;;
            *)
                echo "Unknown event: $event"
                ;;
        esac
    done
}

# Function to terminate all background processes
terminate_background_processes() {
    echo "Terminating background processes..."
    for pid in "${monitor_pids[@]}"; do
        kill "$pid"
    done
    unset monitor_pids
}

# Trap SIGINT signal (Ctrl+C) to terminate background processes before exiting
trap terminate_background_processes SIGINT

while true; do
    readarray -t players < <(playerctl -l)

    for player in "${players[@]}"; do
        monitor_player "$player" &
        monitor_pids+=("$!")
    done

    echo "Monitor player processes started: ${monitor_pids[*]}"

    sleep 10

    terminate_monitor_processes
done
