#!/usr/bin/env bash

#initial check for occupied workspaces
for num in $(hyprctl workspaces | grep ID | sed 's/()/(1)/g' | awk 'NR>1{print $1}' RS='(' FS=')'); do 
  export o"$num"="$num"
done
 
#initial check for focused workspace
for num in $(hyprctl monitors | grep active | sed 's/()/(1)/g' | awk 'NR>1{print $1}' RS='(' FS=')'); do 
  export f"$num"="$num"
  export fnum=f"$num"
done

render() {

    result=""
    
    # Loop over each workspace
    for ((workspace = 1; workspace <= $workspaces; workspace++)); do
        result2=""

        clients=$(hyprctl clients -j | gojq --argjson ws "$workspace" '.[] | select(.workspace.id == $ws) | [.initialClass, .initialTitle]' -c | tr -d "[\"]")

        if [[ "" == $clients ]]; then
            if [[ $workspace == $(hyprctl activeworkspace -j | gojq .id) ]]; then
                result="$result (button :class \"selected\" :onclick \"hyprctl dispatch workspace $workspace\" (box (box :class \"app-img\" :width 60 :style \"background-image: url('./images/notFound.png');\")))"
            else
                result="$result (button :class \"deselected\" :onclick \"hyprctl dispatch workspace $workspace\" (box (box :class \"app-img\" :width 60 :style \"background-image: url('./images/notFound.png');\")))"
            fi
            continue;
        fi

        # Get the list of clients in the workspace
        while IFS= read -r line; do
            class=$(echo "$line" | cut -d ',' -f 1)
            title=$(echo "$line" | cut -d ',' -f 2)

            icon=$(./scripts/getIcon.sh "$class" "$title")
            result2="$result2 (box :class \"app-img\" :width 60 :style \"background-image: url('$icon');\")"
        done <<< $clients

        if [[ $workspace == $(hyprctl activeworkspace -j | gojq .id) ]]; then
            result="$result (button :class \"selected\" :onclick \"hyprctl dispatch workspace $workspace\" (box $result2))"
        else
            result="$result (button :class \"deselected\" :onclick \"hyprctl dispatch workspace $workspace\" (box $result2))"
        fi
    done

    echo "(box :class \"genwin\" :space-evenly false :height 60 :orientation \"h\" $result)"
}

# Get the list of workspaces
workspaces=$(hyprctl workspaces -j | gojq 'sort_by(.id).[].id' | tail -n 1)
render "$event"

socat -u UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/"$HYPRLAND_INSTANCE_SIGNATURE"/.socket2.sock - | while read -r event; do
    workspaces=$(hyprctl workspaces -j | gojq 'sort_by(.id).[].id' | tail -n 1)
    render "$event"
done
