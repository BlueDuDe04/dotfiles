#!/usr/bin/env bash

hyprctl dispatch submap reset

eww close nav-bar shortcut-bar

hyprctl keyword general:gaps_out 0
hyprctl keyword general:gaps_in 0
hyprctl keyword general:border_size 0
hyprctl keyword decoration:rounding 0

if [ "0" == "$(hyprctl activeworkspace -j | gojq '.windows')" ]; then
    nwg-drawer -mt 100 -mr 100 -mb 100 -ml 100 -nocats -nofs -wm 'hyprland';
fi
