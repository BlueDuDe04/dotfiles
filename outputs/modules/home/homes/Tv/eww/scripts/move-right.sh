#!/usr/bin/env bash

if [ "$(hyprctl activeworkspace -j | gojq .id)" -lt "$(($(hyprctl clients -j | gojq 'sort_by(.workspace.id).[].workspace.id' | tail -n 1) + 1))" ]; then
    if [[ $1 == "workspace" ]]; then
        hyprctl dispatch workspace r+1;
    else
        hyprctl dispatch movetoworkspace r+1;
    fi
fi
