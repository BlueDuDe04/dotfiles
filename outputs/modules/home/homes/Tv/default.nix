{
  ...
}:

{
  inputs,
  pkgs,
  ...
}:

let
  font = pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; };
in

{
  home = {
    file = {
      ".local/share/jellyfinmediaplayer/scripts/mpris.so".source = "${pkgs.mpvScripts.mpris}/share/mpv/scripts/mpris.so";
    };

    packages = with pkgs; [
      font

      gojq
      socat

      (stdenv.mkDerivation {
        inherit (pavucontrol) pname version;

        src = pavucontrol;
        icon = ./audio-icon.png;

        installPhase = ''
          mkdir -p $out/share/icons/hicolor/512x512/apps/

          for item in $src/*; do
            if [ ! "$item" == "$src/share" ]; then
              ln -s $src/$(basename $item) $out/$(basename $item)
            fi
          done

          for item in $src/share/*; do
            ln -s $src/share/$(basename $item) $out/share/$(basename $item)
          done

          ln -s $icon $out/share/icons/hicolor/512x512/apps/multimedia-volume-control.png
        '';
      })

      nwg-drawer
      eww

      # Jellyfin Tv
      (stdenv.mkDerivation {
        name = "jellyfin-media-player";

        src = jellyfin-media-player;
        bin = writeShellScriptBin "jellyfinmediaplayer" ''${jellyfin-media-player}/bin/jellyfinmediaplayer --tv'';

        installPhase = /* bash */ ''
          mkdir -p $out

          for item in $src/*; do
            if [ ! "$item" == "$src/bin" ]; then
              ln -s $src/$(basename $item) $out/$(basename $item)
            fi
          done

          ln -s $bin/bin $out/bin
        '';
      })
    ];
  };

  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
  };

  programs = {
    firefox = {
      enable = true;

      profiles.tv = {
        extensions = with inputs.firefox-addons.packages."x86_64-linux"; [
          ublock-origin 
          sponsorblock
          darkreader
        ];

        settings = {
          # Theme
          "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

          # New Tab
          "browser.startup.homepage" = "chrome://browser/content/blanktab.html";
          "browser.newtabpage.enabled" = false;

          # Options
          "browser.fullscreen.autohide" = false;
          "browser.toolbars.bookmarks.visibility" = "never";
          "signon.rememberSignons" = false;
          "browser.download.useDownloadDir" = false;
          "browser.aboutConfig.showWarning" = false;

          # XDG-Desktop-Portal
          "widget.use-xdg-desktop-portal.file-picker" = 1;
          "widget.use-xdg-desktop-portal.mime-handler" = 1;
          "widget.use-xdg-desktop-portal.settings" = 1;
          "widget.use-xdg-desktop-portal.location" = 1;
          "widget.use-xdg-desktop-portal.open-uri" = 1;

          # Security
          "dom.security.https_only_mode" = true;
        };

        search = {
          default = "DuckDuckGo";

          force = true;

          order = [
            "DuckDuckGo"
            "Nix Packages"
            "NixOs Wiki"
          ];

          engines = {
            "Nix Packages" = {
              urls = [{
                template = "https://search.nixos.org/packages";
                params = [
                  { name = "type"; value = "packages"; }
                  { name = "query"; value = "{searchTerms}"; }
                ];
              }];

              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = [ "@np" ];
            };

            "NixOS Wiki" = {
              urls = [{ template = "https://nixos.wiki/index.php?search={searchTerms}"; }];
              iconUpdateURL = "https://nixos.wiki/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000; # every day
              definedAliases = [ "@nw" ];
            };
          };
        };
      };
    };

    # chromium = {
    #   enable = true;
    #
    #   # package = pkgs.ungoogled-chromium;
    #
    #   commandLineArgs = [
    #     "--enable-features=UseOzonePlatform"
    #     "--ozone-platform=wayland"
    #   ];
    #
    #   extensions = [
    #     { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # uBlock Origin
    #     { id = "mnjggcdmjocbbbhaepdhchncahnbgone"; } # SponsorBlock
    #     { id = "eimadpbcbfnmbkopoojfekhnkhdbieeh"; } # Dark Reader
    #   ];
    # };
  };

  wayland.windowManager.hyprland = {
    enable = true;

    extraConfig = let
      onSuper = pkgs.writeShellScriptBin "run" ''
        hyprctl dispatch submap nav

        if [[ "true" == $(hyprctl activeworkspace -j | gojq .hasfullscreen) ]]; then
          hyprctl dispatch fullscreen;
        fi

        if [ "0" == "$(hyprctl activeworkspace -j | gojq '.windows')" ]; then
          pkill nwg-drawer
        fi

        hyprctl keyword general:gaps_out 100, 20, 100, 20
        hyprctl keyword general:gaps_in 10
        hyprctl keyword general:border_size 5
        hyprctl keyword decoration:rounding 6

        eww open-many nav-bar shortcut-bar
      '';

      onEnter = pkgs.writeShellScriptBin "run" (builtins.readFile ./eww/scripts/onEnter.sh);

      onFullscreen = pkgs.writeShellScriptBin "run" ''
        if [ "0" == "$(hyprctl activeworkspace -j | gojq '.windows')" ]; then
          ${onEnter}/bin/run;
        else
          hyprctl dispatch fullscreen
        fi
      '';

      move-right = pkgs.writeShellScriptBin "run" (builtins.readFile ./eww/scripts/move-right.sh);

      move-left = pkgs.writeShellScriptBin "run" ''
        hyprctl dispatch workspace r-1;
      '';
    in /* hyprlang */ ''
      exec-once = nwg-drawer -mt 100 -mr 100 -mb 100 -ml 100 -nocats -nofs -wm 'hyprland'

      exec-once = eww daemon --force-wayland

      bind = SUPER,T,exec,${pkgs.foot}/bin/foot

      # will switch to a submap called resize
      bind = SUPER, SUPER_R, exec, ${onSuper}/bin/run
      bind = SUPER, SUPER_L, exec, ${onSuper}/bin/run

      # will start a submap called "nav"
      submap = nav

      # sets repeatable binds for resizing the active window
      bind = , right, exec, ${move-right}/bin/run workspace
      bind = , left, exec, ${move-left}/bin/run
      bind = Shift, right, exec, ${move-right}/bin/run movetoworkspace
      bind = Shift, left, movetoworkspace, r-1

      bind = , f, exec, ${onFullscreen}/bin/run
      bind = , q, killactive

      # use reset to go back to the global submap
      bind = , return, exec, ${onEnter}/bin/run 

      # will reset the submap, which will return to the global submap
      submap = reset

      # keybinds further down will be global again...

      input {
        kb_layout=us,ru
        kb_options=grp:caps_toggle

        sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
        scroll_factor = 0.2
        touchpad {
          scroll_factor = 0.2
        }
      }

      general {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more
          gaps_in = 0
          gaps_out = 0
          border_size = 0
          col.active_border = rgba(AD8EE6FF) rgba(33ccffFF) 45deg
          col.inactive_border = rgba(595959FF)
      }

      decoration {
          rounding = 0
          
          blur {
              enabled = true
              size = 4
              passes = 5
          }
      }

      misc {
        disable_hyprland_logo = true
        disable_splash_rendering = true
      }
    '';
  };

  xdg.configFile = {
    "eww".source = ./eww;
    "nwg-drawer".source = ./nwg-drawer;
  };
}
