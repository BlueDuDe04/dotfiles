{
  ...
}:

{
  pkgs,
  lib,
  config,
  ...
}:

let
  inherit (lib) types mkOption mkIf;

  cfg = config.myWaybar;
in

{
  options.myWaybar = mkOption {
    type = types.submodule {
      options = {
        enable = mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
    default = {};
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [ waybar ];

    xdg.configFile = {
        "waybar/config".source = ./config;
        "waybar/style.css".source = ./style.css;
    };
  };
}
