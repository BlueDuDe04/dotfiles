{
  ...
}:

{
  pkgs,
  lib,
  config,
  ...
}:

let
  inherit (lib) mkOption types mkIf;

  cfg = config.nvidia;
in

{
  options.nvidia = mkOption {
    type = types.submodule {
      options = {
        enable = mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
    default = {};
  };

  config = mkIf cfg.enable {
    services.xserver = {
      enable = true;
      videoDrivers = [ "nvidia" ];
      excludePackages = [ pkgs.xterm ];
    };

    hardware.nvidia = {
      open = true;
      nvidiaSettings = false;
      modesetting.enable = true;
      powerManagement.enable = false;
    };
    
    environment.sessionVariables = {
      WLR_NO_HARDWARE_CURSORS = "1";
      NIXOS_OZONE_WL = "1";
    };
  };
}
