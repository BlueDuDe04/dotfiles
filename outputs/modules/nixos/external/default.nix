{
  inputs,
  ...
}:

with inputs; {
  home-manager = home-manager.nixosModules.default;
  disko = disko.nixosModules.disko;
  sops-nix = sops-nix.nixosModules.sops;
  impermanence = impermanence.nixosModules.impermanence;
  stylix = stylix.nixosModules.stylix;
  nix-topology = nix-topology.nixosModules.default;
}
