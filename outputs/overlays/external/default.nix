{
  inputs,
  ...
}:

with inputs; {
  sops-nix = sops-nix.overlays.default;
  nixgl = nixgl.overlay;
  nix-topology = nix-topology.overlays.default;
  comfyui = nix-comfyui.overlays.default;
}
