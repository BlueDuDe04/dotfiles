extraSpecialArgs @ {
  inputs,
  system,
  overlays,
  modules,
  ...
}:

let
  nixpkgs = inputs.nixpkgs;

  pkgs = import nixpkgs {
    inherit system;

    config.allowUnfree = true;

    overlays = with overlays; [
      # External
      nixgl

      # Internal
      local
    ];
  };
in

inputs.home-manager.lib.homeManagerConfiguration {
  inherit pkgs extraSpecialArgs;

  modules = with modules.home; [
    # External
    stylix

    # Internal
    utils
    monitors
    tools
    firefox
    waybar
    drata-agent
  ] ++ [
    # Files
    ./config.nix
  ];
}
