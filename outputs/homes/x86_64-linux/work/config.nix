{
  root,
  name,
  system,
  pkgs,
  ...
}:

let
  font = pkgs.nerdfonts.override { fonts = [ "VictorMono" "FiraCode" ]; };
  
  nwg-drawer = pkgs.lib.local.nwg-drawer-builder pkgs {
    args = [
      "-mt 100 -mr 100 -mb 100 -ml 100 -nocats -nofs"
      "-wm 'hyprland'"
    ];

    drawer = ''
      window {
        border-radius: 10px;
      }
    '';
  };
in

{
  home = {
    username = name;
    homeDirectory = "/home/" + name;

    sessionVariables = {
      EDITOR = "nv";
      BROWSER = "firefox";
      TERMINAL = "kitty";
      TERM = "kitty";
      CLIPBOARD_NOGUI = 1;

      XINITRC = "${derivation {
        name = "xinitrc";
        system = system;
        builder = "${pkgs.bash}/bin/bash";
        args = [ "-c" "echo '. /etc/X11/Xsession' > $out" ];
      }}";
    };

    packages = with pkgs; [
      nixgl.nixGLIntel
      nixgl.nixVulkanIntel

      local.nv
      local.slack-wayland
      local.dbeaver

      nwg-drawer
    ] ++ font;
  };

  monitors = [
    {
      name = "eDP-1";
      primary = true;
      wallpaper = {
        img = root + "/extras/wallpapers/Nix-Snowfall.png";
        mode = "fill";
      };
      width = 2256;
      height = 1504;
      scale = "1.175";
    }

    {
      name = "DP-3";
      wallpaper = {
        img = root + "/extras/wallpapers/Nix-Snowfall.png";
        mode = "fill";
      };
      width = 2560;
      height = 1440;
      x = 0;
      y = -1440;
    }
  ];

  stylix = {
    enable = true;

    image = root + "/extras/wallpapers/Nix-Snowfall.png";
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";

    opacity.terminal = 0.9;

    cursor = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
      size = 26;
    };

    fonts = let
      settings = {
        package = font;
        name = "Firacode Nerd Font Mono:style=Bold";
      };
    in {
      serif = settings;
      sansSerif = settings;
      monospace = settings;
    };

    targets = {
      hyprland.enable = false;
      wezterm.enable = false;
      kitty.enable = false;
      fish.enable = false;
      vim.enable = false;
      kde.enable = false;
    };
  };

  xdg = {
    enable = true;

    portal = {
      enable = true;

      xdgOpenUsePortal = true;
      config.common.default = "*";

      # gtk portal needed to make gtk apps happy
      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
        xdg-desktop-portal-hyprland
      ];
    };
  };

  tools = {
    enable = true;
    cmdline = true;
    apps = true;
    wayland = true;
  };

  myFirefox.enable = true;
  myWaybar.enable = true;

  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
  };

  services.syncthing.enable = true;

  programs = {
    kitty = {
      enable = true;

      theme = "Tokyo Night";

      extraConfig = ''
        shell fish
        background_opacity 0.9
        cursor_blink_interval 0
        touch_scroll_multiplier 10.0
        window_padding_width 0 6

        font_family      VictorMono Nerd Font
        bold_font        auto
        italic_font      auto
        bold_italic_font auto
        font_size 13
      '';
    };

    chromium = {
      enable = true;

      # package = pkgs.ungoogled-chromium;

      commandLineArgs = [
        "--enable-features=UseOzonePlatform"
        "--ozone-platform=wayland"
      ];

      extensions = [
        { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # uBlock Origin
        { id = "mnjggcdmjocbbbhaepdhchncahnbgone"; } # SponsorBlock
        { id = "eimadpbcbfnmbkopoojfekhnkhdbieeh"; } # Dark Reader
      ];
    };
  };

  wayland.windowManager.hyprland = {
    enable = true;

    settings = {
      exec = [
        "pkill cliphist ; ${pkgs.wl-clipboard}/bin/wl-paste --watch ${pkgs.cliphist}/bin/cliphist store"
        "pkill waybar ; ${pkgs.waybar}/bin/waybar -c ~/.config/waybar/config -s ~/.config/waybar/style.css"
      ];

      bind = let
        yankSearch = pkgs.writeShellScriptBin "run" ''
          ${pkgs.cliphist}/bin/cliphist list | ${pkgs.fuzzel}/bin/fuzzel -p '❯  ' -b 2A2B3DFF -s AD8EE6FF -t DDDDDDFF -S DDDDDDFF -d | ${pkgs.cliphist}/bin/cliphist decode | ${pkgs.wl-clipboard}/bin/wl-copy
        '';

        yankDisplay = pkgs.writeShellScriptBin "run" ''
          ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp)" - | ${pkgs.wl-clipboard}/bin/wl-copy
        '';

        yankColor = pkgs.writeShellScriptBin "run" ''
          ${pkgs.hyprpicker}/bin/hyprpicker | ${pkgs.wl-clipboard}/bin/wl-copy
        '';

        pasteSearch = pkgs.writeShellScriptBin "run" ''
          ${pkgs.cliphist}/bin/cliphist list | ${pkgs.fuzzel}/bin/fuzzel -p '❯  ' -b 2A2B3DFF -s AD8EE6FF -t DDDDDDFF -S DDDDDDFF -d | ${pkgs.cliphist}/bin/cliphist decode | ${pkgs.wtype}/bin/wtype -
        '';
      in [
        "SUPER, Q, exec, hyprctl dispatch killactive"
        "SUPER_SHIFT, M, exit,"
        "SUPER_SHIFT, F, togglefloating,"
        "SUPER, F, fullscreen"

        "SUPER, y, exec, ${yankSearch}/bin/run"
        "SUPER, d, exec, ${yankDisplay}/bin/run"
        "SUPER, c, exec, ${yankColor}/bin/run"
        "SUPER, p, exec, ${pasteSearch}/bin/run"

        "SUPER, o, exec, nwg-drawer"
        "SUPER, t, exec, kitty"
        "SUPER, w, exec, firefox"
        "SUPER, n, exec, nautilus"

        # Move focus with $SUPER + arrow keys
        "SUPER, left, movefocus, l"
        "SUPER, down, movefocus, d"
        "SUPER, up, movefocus, u"
        "SUPER, right, movefocus, r"

        "SUPER_SHIFT, left, movewindoworgroup, l"
        "SUPER_SHIFT, down, movewindoworgroup, d"
        "SUPER_SHIFT, up, movewindoworgroup, u"
        "SUPER_SHIFT, right, movewindoworgroup, r"

        ", XF86AudioRaiseVolume, exec, ${pkgs.pamixer}/bin/pamixer -i 10"
        ", XF86AudioLowerVolume, exec, ${pkgs.pamixer}/bin/pamixer -d 10"
        ", XF86AudioMute, exec, ${pkgs.pamixer}/bin/pamixer --toggle-mute"
        ", XF86MonBrightnessUp, exec, ${pkgs.brightnessctl}/bin/brightnessctl s 10%+"
        ", XF86MonBrightnessDown, exec, ${pkgs.brightnessctl}/bin/brightnessctl s 10%-"
      ] ++ (
        # workspaces
        # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
        builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
            in [
              "SUPER, ${ws}, workspace, ${toString (x + 1)}"
              "SUPER_SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          10)
      );
    };

    extraConfig = ''
      bindm = ALT,mouse:272,movewindow
      bindm = ALT,mouse:273,resizewindow

      bindm = ,mouse:276,movewindow
      bindm = ,mouse:275,resizewindow

      bind = SUPER+ALT, left, changegroupactive, b
      bind = SUPER+ALT, right, changegroupactive, f

      bind = SUPER_CONTROL, right, resizeactive, 50 0
      bind = SUPER_CONTROL, down, resizeactive, 0 50
      bind = SUPER_CONTROL, up, resizeactive, 0 -50
      bind = SUPER_CONTROL, left, resizeactive, -50 0

      bind = SUPER_SHIFT, T, togglegroup
      bind = SUPER_SHIFT, O, togglesplit, # dwindle
      bind = SUPER_SHIFT, P, pseudo, # dwindle

      windowrule = float,^(org.gnome.Nautilus)$
      windowrule = float,^(org.gnome.Calculator)$
      windowrule = float,^(zenity)$

      input {
        repeat_delay=200
        repeat_rate=60

        touchpad {
          natural_scroll = yes
        }

        sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
      }

      general {
          gaps_in = 10
          gaps_out = 10
          border_size = 5
          col.active_border = rgba(AD8EE6FF) rgba(33ccffFF) 45deg
          col.inactive_border = rgba(595959FF)

          layout = dwindle
      }

      decoration {
          rounding = 6
          
          blur {
              enabled = true
              size = 4
              passes = 5
          }

          drop_shadow = no
          shadow_range = 4
          shadow_render_power = 3
          col.shadow = rgba(1a1a1aee)
      }

      animations {
          enabled = yes

          bezier = myBezier, 0.05, 0.9, 0.1, 1.05

          animation = windows, 1, 7, myBezier
          animation = windowsOut, 1, 7, default, popin 80%
          animation = border, 1, 10, default
          animation = borderangle, 1, 8, default
          animation = fade, 1, 7, default
          animation = workspaces, 1, 6, default
      }

      group {
          col.border_active = rgba(AD8EE6FF) rgba(33ccffFF) 45deg
          col.border_inactive = rgba(595959FF)

          groupbar {
            # height = 18
            render_titles = false
            font_family = "FiraCode Nerd Font Bold"
            font_size = 10
            gradients = false

            col.active = rgba(AD8EE6FF)
            col.inactive = rgba(595959FF)
          }
      }

      dwindle {
          pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
          preserve_split = yes # you probably want this
          force_split = 2
          no_gaps_when_only = 1
      }

      gestures {
          workspace_swipe = true
          workspace_swipe_forever = true
      }

      misc {
        disable_hyprland_logo = true
        disable_splash_rendering = true

        enable_swallow = true
        swallow_regex = ^(kitty|FreeTube)$
      }

      xwayland {
        force_zero_scaling = true
      }
    '';
  };
}
