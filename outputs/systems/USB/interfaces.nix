{
  name ? "interfaces",
  pkgs,
  ...
}:

pkgs.writeCBin name /* c */ ''
  #define ADDRESS "192.168.0.22"
  #define PORT 8787
  #include "${./interfaces.c}"
''
