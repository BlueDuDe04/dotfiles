{
  name,
  system,
  pkgs,
  lib,
  ...
}:

{
  networking = {
    hostName = name;

    networkmanager.enable = true;
  };

  # security = {
  #   sudo.enable = lib.mkForce false;
  #   doas = {
  #     enable = true;
  #
  #     wheelNeedsPassword = true;
  #
  #     extraRules = [{
  #       users = [ "mason" ];
  #       keepEnv = true;
  #       persist = true;
  #     }];
  #   };
  # };

  nix.settings.allowed-users = [ "root" ];

  users = {
    mutableUsers = false;

    users = {
      root.initialPassword = "test";


      mason = {
        isNormalUser = true;
        extraGroups = [ "networkmanager" "wheel" ];
        initialPassword = "test";

        # openssh.authorizedKeys.keys = [
        #   "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFSRSgJPxEgxMDLE1BrwKiz/QF+b8ywAQbkaG5cAp79J Bennettmason04@gmail.com"
        # ];
      };
    };
  };

  systemd.services.listen = {
    serviceConfig.ExecStart = "${import ./interfaces.nix { inherit system pkgs; }}/bin/interfaces";

    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    wantedBy = [ "multi-user.target" ];
  };


  services = {
    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
    };
  };

  environment = {
    defaultPackages = lib.mkForce [ ];

    variables = {
      dotfiles = "gitlab:BlueDuDe04/dotfiles";
    };

    systemPackages = with pkgs; [
      disko
      # doas-sudo-shim
    ];
  };
}
