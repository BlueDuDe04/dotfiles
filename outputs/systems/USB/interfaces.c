#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>

#include <assert.h>

#include <sys/types.h>
#include <ifaddrs.h>

#include <sys/socket.h>
#include <netdb.h>

#ifndef ADDRESS
#error "ADDRESS is not defined"
#endif

#ifndef PORT
#error "PORT is not defined"
#endif

#ifndef HELLO
#define HELLO "HELLO"
#endif

int main(int argc, char const* argv[]) {
  printf("%s: %s, %d\n", HELLO, ADDRESS, PORT);

  int status, valread, client_fd;
  struct sockaddr_in serv_addr;

  if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      printf("\n Socket creation error \n");
      return -1;
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  // Convert IPv4 and IPv6 addresses from text to binary form
  if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0) {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if ((status = connect(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr))) < 0) {
    printf("\nConnection Failed \n");
    return -1;
  }

  // interfaces
  struct ifaddrs *ifaddr, *ifa;
  int family, s;
  char host[NI_MAXHOST + 1];

  if (getifaddrs(&ifaddr) == -1) {
    perror("getifaddrs");
    exit(EXIT_FAILURE);
  }

  for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr == NULL)
      continue;  

    s = getnameinfo(
      ifa->ifa_addr,
      sizeof(struct sockaddr_in),
      host,
      NI_MAXHOST,
      NULL,
      0,
      NI_NUMERICHOST
    );

    if (ifa->ifa_addr->sa_family == AF_INET) {
      if (s != 0) {
        printf("getnameinfo() failed: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
      }

      uint32_t send_size = strlen(host);
      host[send_size] = ',';
      send(client_fd, host, send_size + 1, 0);
    }
  }
  char end = 0;
  send(client_fd, &end, 1, 0);

  freeifaddrs(ifaddr);
  exit(EXIT_SUCCESS);

  close(client_fd);

  return 0;
}
