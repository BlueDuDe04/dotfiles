{
  ...
}:

{
  sops = {
    age.keyFile = "/nix/persist/system/var/lib/sops-nix/age/keys.txt";

    secrets = {
      root = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      mason = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      work = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      share = {
        sopsFile = ./smb-secrets;
        format = "binary";
      };

      wireless = {
        sopsFile = ./wireless.conf;
        format = "binary";
      };

      private-key = {
        sopsFile = ./wireguard.yaml;
      };

      preshared-key = {
        sopsFile = ./wireguard.yaml;
      };
    };
  };
}
