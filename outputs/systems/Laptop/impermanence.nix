{
  ...
}:

{
  environment.persistence."/nix/persist/system" = {
    enable = true;
    hideMounts = true;

    directories = [
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"

      # Nix
      "/root/.cache/nix"
      "/root/.local/share/nix"

      # Tmp
      "/tmp"
      "/var/tmp"
    ];

    files = [
      "/var/lib/sops-nix/age/keys.txt"

      # Sudo
      "/var/db/sudo/lectured/1000"
    ];
  };
}
