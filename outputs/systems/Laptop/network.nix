let
  sambaHost = "192.168.0.22";
  automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
  permissions = "file_mode=0660,dir_mode=0770";
in 

{
  name,
  pkgs,
  config,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    # For mount.cifs, required unless domain name resolution is not needed.
    cifs-utils
    nfs-utils
    wpa_supplicant_gui
  ];

  networking = {
    hostName = name;

    wireless = {
      enable = true;
      userControlled.enable = true;

      secretsFile = config.sops.secrets.wireless.path;

      interfaces = [ "wlp0s20f3" ];

      networks = {
        Telus-E51290-5G = {
          pskRaw = "ext:Telus-E51290-5G";
          priority = 2;
        };

        "Telus-E51290-2.4G" = {
          pskRaw = "ext:Telus-E51290-2.4G";
          priority = 1;
        };

        SHAW-D7EA = {
          pskRaw = "ext:SHAW-D7EA";
          priority = 1;
        };

        SFUNET-SECURE.auth = /* lua */ ''
          key_mgmt=WPA-EAP
          eap=PEAP
          identity="mba182@sfu.ca"
          password=ext:SFUNET-SECURE
          priority=1
        '';
      };
    };

    wg-quick = {
      interfaces.HomeVPN = {
        autostart = false;
        address = [ "10.0.0.2/24" ];
        privateKeyFile = config.sops.secrets.private-key.path;

        peers = [{
          publicKey = "UfXC+ARAOysLwdmNEBrkiVziW3Rnm9msRAA9ERe4tD4=";
          presharedKeyFile = config.sops.secrets.preshared-key.path;
          allowedIPs = [ "192.168.0.0/24" ];
          endpoint = "23.16.232.15:51820";
          persistentKeepalive = 25;
        }];
      };
    };
  };

  users.groups = {
    share = {
      gid = 4321;
      members = [ "mason" ];
    };
  };

  users.users.mason.extraGroups = [ "share" ];

  fileSystems = {
    "/mnt/share/server/combined" = {
      device = "//${sambaHost}/Combined";
      fsType = "cifs";
      options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
    };

    "/mnt/share/server/backup" = {
      device = "//${sambaHost}/Backup";
      fsType = "cifs";
      options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
    };

    "/mnt/share/server/merged" = {
      device = "//${sambaHost}/Merged";
      fsType = "cifs";
      options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
    };

    "/mnt/share/server-nfs" = {
      device = "192.168.0.22:/mnt";
      fsType = "nfs";
      options = [ "${automount_opts}" ];
    };
  };
}
