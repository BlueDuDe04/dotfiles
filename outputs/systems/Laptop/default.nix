{
  root,
  recImportDirectories,
  name,
  inputs,
  ...
}:

let
  system = "x86_64-linux";

  internal = recImportDirectories root (name: file: path: (import file {
    inherit root name inputs system;
  }));

  nixpkgs = inputs.nixpkgs;

  pkgs = import nixpkgs {
    inherit system;

    config.allowUnfree = true;

    overlays = [
      (final: prev: { local = internal.pkgs; lib = prev.lib // { local = internal.lib; }; })
    ];
  };

  nixosSystem = nixpkgs.lib.nixosSystem {
    inherit system pkgs;

    specialArgs = {
      inherit root name inputs system;
      modules = internal.modules;
    };

    modules = with internal.modules.nixos; [
      external.sops-nix
      external.home-manager
      external.stylix
      external.disko
      external.impermanence
      external.nix-topology

      setup
    ] ++ [
      # Directories
      ./homes
      ./secrets

      # Files
      ./config.nix
      ./hardware.nix
      ./disko.nix
      ./impermanence.nix
      ./network.nix
    ];
  };
in

(internal.lib.nixos-build pkgs nixosSystem) // {
  inherit nixosSystem;

  disko = import ./disko.nix {};

  bar = import ./bar {
    inherit system pkgs;
  };
}
