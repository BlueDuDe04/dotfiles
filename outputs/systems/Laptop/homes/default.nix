{
  root,
  inputs,
  system,
  modules,
  config,
  local,
  ...
}:

{
  imports = [
    ./mason
  ];

  users.users.root.hashedPasswordFile = config.sops.secrets.root.path;

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    extraSpecialArgs = {
      inherit root inputs system modules local;
    };

    sharedModules = with modules.home; [
      utils
    ];
  };
}
