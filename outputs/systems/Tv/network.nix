{
  name,
  ...
}:

{
  networking = {
    hostName = name;

    networkmanager.enable = true;

    # defaultGateway = {
    #   address = "192.168.0.1";
    #   interface = "enp2s0";
    # };
    #
    # nameservers = [
    #   "192.168.0.1"
    # ];
    #
    # interfaces.enp2s0.ipv4.addresses = [
    #   {
    #     address = "192.168.0.44";
    #     prefixLength = 24;
    #   }
    # ];
  };
}
