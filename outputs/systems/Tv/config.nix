{
  root,
  system,
  pkgs,
  ...
}:

let
  wallpaper = derivation {
    name = "wallpaper.jpg";
    inherit system;
    builder = "${pkgs.bash}/bin/bash";
    args = [ "-c" ''
      export PATH=${with pkgs; lib.makeBinPath [
        coreutils
      ]}

      ${pkgs.lutgen}/bin/lutgen apply -p catppuccin-mocha ${root + "/extras/wallpapers/Sunset-Deer-Wallpaper.jpg"} -o $out
    '' ];
  };
in

{
  nix = {
    gc = {
      automatic = false;
      dates = "daily";
      options = "--delete-older-than 7d";
    };

    settings = {
      auto-optimise-store = false;

      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  # Set your time zone.
  time.timeZone = "America/Vancouver";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  documentation.nixos.enable = false;

  # Do not change unless you know what you are doing!
  system.stateVersion = "22.11"; # Did you read the comment?

  security = {
    polkit.enable = true;

    sudo.enable = false;
  };

  services = {
    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
    };

    pipewire = {
      enable = true;
      audio.enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;
    };

    greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "Hyprland";
          user = "tv";
        };
      };
    };
  };

  stylix = {
    enable = true;
    autoEnable = true;

    homeManagerIntegration = {
      autoImport = true;
      followSystem = true;
    };

    image = wallpaper;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";

    opacity.terminal = 0.9;

    cursor = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
      size = 26;
    };

    fonts = let
      settings = {
        package = pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; };
        name = "Firacode Nerd Font Mono:style=Bold";
      };
    in {
      serif = settings;
      sansSerif = settings;
      monospace = settings;
    };
  };
}
