{
  modules,
  pkgs,
  ...
}:

{
  imports = with modules.home; [
    # External

    # Internal
    homes.Tv
  ];

  home = {
    stateVersion = "22.11"; # Please read the comment before changing.

    username = "tv";
    homeDirectory = "/home/tv";

    packages = with pkgs; [
      local.plex-htpc
      local.IPTVnator
    ];
  };

  stylix.targets = {
    swaylock.enable = false;
    wezterm.enable = false;
    kitty.enable = false;
    fish.enable = false;
    vim.enable = false;
    kde.enable = false;
  };

  wayland.windowManager.hyprland.settings = {
    monitor = [
      # ",disable"
      "HDMI-A-1,1920x1080@60,0x0,1"
    ];
  };
}
