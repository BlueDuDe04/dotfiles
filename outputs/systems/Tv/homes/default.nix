{
  root,
  inputs,
  system,
  modules,
  extras,
  ...
}:

{
  users.users = {
    tv = {
      isNormalUser = true;
      initialPassword = "test";
    };
  };

  home-manager = {
    users = {
      tv = ./tv;
    };

    useGlobalPkgs = true;
    useUserPackages = true;

    extraSpecialArgs = {
      inherit root inputs system modules extras;
    };

    sharedModules = with modules.home; [
    ];
  };
}
