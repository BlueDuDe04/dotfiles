{
  ...
}:

{
  sops = {
    age.keyFile = "/var/lib/sops-nix/age/keys.txt";

    secrets.root = {
      sopsFile = ./passwords.yaml;
      neededForUsers = true;
    };

    secrets.tv = {
      sopsFile = ./passwords.yaml;
      neededForUsers = true;
    };
  };
}
