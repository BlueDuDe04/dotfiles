{
  modules,
  pkgs,
  ...
}:

{
  imports = with modules.home; [
    # External

    # Internal
    homes.Tv
  ];

  home = {
    stateVersion = "22.11"; # Please read the comment before changing.

    username = "tv";
    homeDirectory = "/home/tv";

    packages = with pkgs; [
      local.plex-htpc
      local.IPTVnator

      freetube
    ];
  };

  wayland.windowManager.hyprland.settings = {
    monitor = [
      ",disable"
      "HDMI-A-1,3840x2160@30,0x0,2,bitdepth,10"
    ];
  };
}
