{
  root,
  inputs,
  system,
  modules,
  config,
  ...
}:

{
  users.users = {
    root.hashedPasswordFile = config.sops.secrets.root.path;

    tv = {
      isNormalUser = true;
      hashedPasswordFile = config.sops.secrets.tv.path;
    };
  };

  home-manager = {
    users = {
      tv = ./tv;
    };

    useGlobalPkgs = true;
    useUserPackages = true;

    extraSpecialArgs = {
      inherit root inputs system modules;
    };

    sharedModules = with modules.home; [
    ];
  };
}
