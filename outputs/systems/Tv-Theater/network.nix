{
  name,
  ...
}:

let
  interface = "enp0s31f6";
in

{
  networking = {
    hostName = name;

    useDHCP = true;

    # defaultGateway = {
    #   inherit interface;
    #   address = "192.168.0.1";
    # };
    #
    # nameservers = [
    #   "192.168.0.1"
    # ];
    #
    # interfaces.${interface}.ipv4.addresses = [{
    #   address = "192.168.0.25";
    #   prefixLength = 24;
    # }];
  };
}
