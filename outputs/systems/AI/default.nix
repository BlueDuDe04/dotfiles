let
  system = "x86_64-linux";
in

args @ {
  root,
  recImportDirectories,
  inputs,
  ...
}:

let
  internal = recImportDirectories root (name: file: path: (import file {
    inherit root recImportDirectories name inputs system;
  }));

  nixpkgs = inputs.nixpkgs;

  pkgs = import nixpkgs {
    inherit system;

    config = {
      allowUnfree = true;

      cudaSupport = true;
    };

    overlays = [
      internal.overlays.external.comfyui

      # (final: prev: { local = internal.pkgs; lib = prev.lib // { local = internal.lib; }; })
    ];
  };

  nixosSystem = nixpkgs.lib.nixosSystem {
    inherit system pkgs;

    specialArgs = args // {
      inherit system;
      modules = internal.modules;
    };

    modules = with internal.modules.nixos; [
      external.disko
      # external.nix-topology
    ] ++ [
      # Directories

      # Files
      ./config.nix
      ./hardware.nix
      ./network.nix
    ];
  };
in {
  inherit nixosSystem;

  btcli = pkgs.python3Packages.buildPythonApplication rec {
    pname = "bittensor";
    version = "8.5.2";
    pyproject = true;

    src = pkgs.fetchPypi {
      inherit pname version;
      sha256 = "sha256-Ktz9R7AWzkmc2fN48NVWLKVdQIARKMoSQYq/atoUiYs=";
    };

    build-system = [
      pkgs.python3Packages.setuptools
    ];
  };
} // (internal.lib.nixos-build pkgs nixosSystem)
