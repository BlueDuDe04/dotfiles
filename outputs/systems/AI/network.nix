let
  address = "192.168.0.120";
  interface = "eno1";

in

{
  name,
  pkgs,
  ...
}:

let
  comfyui = pkgs.comfyuiPackages.comfyui.override {
    extensions = with pkgs.pkgs.comfyuiPackages.extensions; [
      acly-inpaint
      acly-tooling
      cubiq-ipadapter-plus
      fannovel16-controlnet-aux
    ];
  };
in

{
  networking = {
    hostName = name;

    defaultGateway = {
      inherit interface;
      address = "192.168.0.1";
    };

    nameservers = [
      "192.168.0.1"
    ];

    interfaces = {
      ${interface}.ipv4.addresses = [{
        inherit address;
        prefixLength = 24;
      }];
    };

    firewall = {
      enable = true;

      allowedTCPPorts = [
        8188

        8000
      ];

      allowedUDPPorts = [
        8188

        8000
      ];
    };
  };

  services.nginx = {
    enable = true;

    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    virtualHosts.${address} =  {
      locations."/" = {
        proxyPass = "http://127.0.0.1";
        proxyWebsockets = true;
      };
    };
  };

  environment.systemPackages = [
    comfyui
 ];

  systemd.services = {
    comfyui = {
      serviceConfig = {
        WorkingDirectory = "/home/ai/comfyui";
        ExecStart = "${comfyui}/bin/comfyui --listen ${address}";

        User = "ai";
        Group = "users";
      };

      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
    };
  };
}
