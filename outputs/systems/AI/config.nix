{
  system,
  pkgs,
  ...
}:

{
  nix = {
    gc = {
      automatic = false;
      dates = "daily";
      options = "--delete-older-than 7d";
    };

    settings = {
      auto-optimise-store = true;

      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  # Set your time zone.
  time.timeZone = "America/Vancouver";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  documentation.nixos.enable = false;

  # Do not change unless you know what you are doing!
  system.stateVersion = "22.11"; # Did you read the comment?

  users.users = {
    root.initialPassword = "test";

    ai = {
      isNormalUser = true;
      initialPassword = "test";
      extraGroups = [ "wheel" "docker" ];
    };
  };

  security = {
    polkit.enable = true;

    sudo.enable = true;
  };

  services = {
    greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd Hyprland";
          user = "greeter";
        };
      };
    };

    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
    };
  };

  virtualisation.docker = {
    enable = true;
    daemon.settings.features.cdi = true;
    rootless.daemon.settings.features.cdi = true;
  };

  systemd.services.interfaces = {
    serviceConfig.ExecStart = "${import ./interfaces.nix { inherit system pkgs; }}/bin/interfaces";

    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    wantedBy = [ "multi-user.target" ];
  };
}
