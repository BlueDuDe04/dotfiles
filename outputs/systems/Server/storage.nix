{
  pkgs,
  ...
}:

let
  inherit (builtins) concatStringsSep attrNames listToAttrs;

  mount = "/mnt";

  disks = {
    "${mount}/disks/1_3.4T" = {
      device = "/dev/disk/by-uuid/16cf04d0-bbd0-41ea-b253-58179ce2d15d";
      options = [ "nofail" ];
    };

    "${mount}/disks/2_3.4T" = {
      device = "/dev/disk/by-uuid/d916bad9-8e9d-4916-be1d-2f2f5ef62f54";
      options = [ "nofail" ];
    };

    "${mount}/disks/3_1.7T" = {
      device = "/dev/disk/by-uuid/4994914d-7b70-49cd-9436-0d03938da814";
      options = [ "nofail" ];
    };

    "${mount}/disks/4_1.7T" = {
      device = "/dev/disk/by-uuid/c64ab793-5578-4200-a85b-c6cf5c84c92a";
      options = [ "nofail" ];
    };

    "${mount}/disks/5_1.7T" = {
      device = "/dev/disk/by-uuid/756ebaad-a4b0-4cba-ad64-d284589839cf";
      options = [ "nofail" ];
    };

    "${mount}/disks/6_1.7T" = {
      device = "/dev/disk/by-uuid/787921e8-9385-4dd4-b698-f4bf0cdbea77";
      options = [ "nofail" ];
    };
  };

  backup = {
    "${mount}/backup" = {
      device = "/dev/disk/by-uuid/ce1df2d5-212b-4aad-8876-0e75164a3090";
      options = [ "nofail" ];
    };
  };

  binds = [
    {
      dir = "git";
      backup = true;
      share = false;
      Mfsid = "6a5dda1c-c076-47df-8ce4-5d60f5958c3e";
      Cfsid = "9f789995-a9ea-4c27-9411-a7f8481bde21";
    }
    {
      dir = "storage";
      backup = true;
      share = true;
      Mfsid = "b6b597cc-d629-4d7d-b2be-e73e7016946c";
      Cfsid = "5346113a-d6f2-4c05-8a49-b09729a3c56b";
    }
    {
      dir = "gameyfin";
      backup = true;
      share = true;
      Mfsid = "d57f4df8-b962-48bf-8876-39b878491351";
      Cfsid = "2d009b3a-033f-4e0e-a6a3-c5294de73e2c";
    }
    {
      dir = "syncthing";
      backup = true;
      share = true;
      Mfsid = "a3c78e6d-8843-4542-b7cb-fde651774406";
      Cfsid = "048228b8-66c3-4849-84bb-b7b5f8d8946a";
    }
    {
      dir = "transmission";
      backup = true;
      share = true;
      Mfsid = "dd82a052-ca8a-4e35-bd06-eb82bd573cb4";
      Cfsid = "c59f1bd0-6dcb-4347-81a4-f077a115306b";
    }
    {
      dir = "jellyfin/Content";
      backup = true;
      share = true;
      Mfsid = "e4e2360b-9a96-4182-a5de-f93fbce47010";
      Cfsid = "e7cd120e-45e0-49c5-9106-fc1203a511b5";
    }
    {
      dir = "jellyfin/Metadata";
      backup = false;
      share = true;
      Mfsid = "58c4e854-2be2-4f04-b38b-68054036ab60";
      Cfsid = "e3d53e93-42e1-484f-9bdd-98fc5bf62ef7";
    }
  ];

  mounts = listToAttrs (pkgs.lib.flatten (map (item: [
    {
      name = "${mount}/${item.dir}/merged";
      value = {
        fsType = "fuse.mergerfs";
        device = concatStringsSep ":" (map (_:_ + "/${item.dir}") (attrNames disks));
        options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=mfs" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
        depends = attrNames disks;
      };
    }
    {
      name = "${mount}/${item.dir}/backup";
      value = {
        fsType = "none";
        device = "${mount}/backup/${item.dir}";
        options = [ "nofail" "bind" ];
        depends = [ "${mount}/backup" ];
      };
    }
    {
      name = "${mount}/${item.dir}/combined";
      value = {
        fsType = "fuse.mergerfs";
        device = if item.backup == true then
          "${mount}/${item.dir}/backup:${mount}/${item.dir}/merged"
        else
          "${mount}/${item.dir}/merged:${mount}/${item.dir}/backup";
        options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=ff" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
        depends = [ "${mount}/${item.dir}/merged" "${mount}/${item.dir}/backup" ];
      };
    }
  ]) binds));

  shares = concatStringsSep "\n" (map (item: if item.share == false then "" else ''
    ${mount}/${item.dir}/backup 192.168.0.22/24(insecure,rw,sync,no_subtree_check)
    ${mount}/${item.dir}/merged 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=${item.Mfsid})
    ${mount}/${item.dir}/combined 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=${item.Cfsid})
  '') binds);

  share-disks = concatStringsSep "\n" (map (item:
    "${item} 192.168.0.22/24(insecure,rw,sync,no_subtree_check)"
  ) (attrNames disks));
in

{
  fileSystems = disks // backup // mounts // {
    "${mount}/merged" = {
      fsType = "fuse.mergerfs";
      device = concatStringsSep ":" (attrNames disks);
      options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=mfs" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
      depends = attrNames disks;
    };

    "${mount}/combined" = {
      fsType = "fuse.mergerfs";
      device = "${mount}/backup:${mount}/merged";
      options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=ff" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
      depends = [ "${mount}/backup" "${mount}/merged" ];
    };

    "${mount}/jellyfin/Media" = {
      fsType = "fuse.mergerfs";
      device = "${mount}/jellyfin/Metadata/combined:${mount}/jellyfin/Content/combined";
      options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=ff" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
      depends = [ "${mount}/jellyfin/Metadata/combined" "${mount}/jellyfin/Content/combined" ];
    };

    "${mount}/jellyfin/cacheDir" = {
      fsType = "fuse.mergerfs";
      device = concatStringsSep ":" (map (_:_ + "/jellyfin/cacheDir") (attrNames (disks // backup)));
      options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=mfs" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
      depends = attrNames (disks // backup);
    };

    "${mount}/jellyfin/dataDir" = {
      fsType = "fuse.mergerfs";
      device = concatStringsSep ":" (map (_:_ + "/jellyfin/dataDir") (attrNames (disks // backup)));
      options = [ "cache.files=partial" "dropcacheonclose=true" "category.create=mfs" "func.getattr=newest" "noforget" "inodecalc=path-hash" ];
      depends = attrNames (disks // backup);
    };
  };

  services.nfs = {
    server = {
      enable = true;
      lockdPort = 4001;
      mountdPort = 4002;
      statdPort = 4000;
      exports = ''
        ${mount}/backup 192.168.0.22/24(insecure,rw,sync,no_subtree_check)
        ${mount}/merged 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=e6373563-e22b-4b5b-87e9-2e8e771acac5)
        ${mount}/combined 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=7ebed3b5-4558-4371-941a-40e381cc802e)

        ${mount}/jellyfin/Media 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=20be0d8c-11b6-464b-921f-d08dc27300c1)
      '' + shares + "\n" + share-disks;
    };
  };
}
