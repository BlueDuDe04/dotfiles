{
  name ? "listen",
  system ? builtins.currentSystem,
  pkgs ? import <nixpkgs> { inherit system; },
  ...
}:

derivation {
  inherit name system;

  builder = "/bin/sh";

  args = [ "-c" /* sh */ ''
    export PATH=${with pkgs; lib.makeBinPath [
      coreutils
      clang
    ]}

    mkdir -p $out/bin

    clang ${./listen.c} -o $out/bin/${name}
  '' ];
}
