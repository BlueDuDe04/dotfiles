{
  config,
  ...
}:

{
  imports = [
    ./server
  ];

  users = {
    mutableUsers = false;

    users.root.hashedPasswordFile = config.sops.secrets.root.path;
  };
}
