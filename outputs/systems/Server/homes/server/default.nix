{
  config,
  ...
}:

{
  users.users.server = {
    isNormalUser = true;
    extraGroups = [ "wheel" "git" ];
    hashedPasswordFile = config.sops.secrets.server.path;
  };

  environment.persistence."/persist/system".users.server = {
    directories = [
      "Persist"

      { directory = ".ssh"; mode = "0700"; }

      # Nix
      ".cache/nix"
      ".local/share/nix"

      # Direnv
      ".local/share/direnv"

      # Neovim
      ".cache/nvim"
      ".local/share/nvim"
      ".local/state/nvim"

      # Zoxide
      ".local/share/zoxide"
    ];

    files = [
      # Sops Keys
      ".config/sops/age/keys.txt"

      # Fish
      ".local/share/fish/fish_history"
    ];
  };
}
