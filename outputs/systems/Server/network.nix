{
  name,
  pkgs,
  config,
  ...
}:

let
  interface = "enp2s0";
  wg0Port = 51820;
in

{
  networking = {
    hostName = name;

    useDHCP = true;

    defaultGateway = {
      inherit interface;
      address = "192.168.0.1";
    };

    nameservers = [
      "192.168.0.1"
    ];

    interfaces.${interface}.ipv4.addresses = [
      {
        address = "192.168.0.22";
        prefixLength = 24;
      }
    ];

    firewall = {
      enable = true;
      allowedTCPPorts = [ 
        # Syncthing & Gameyfin
        8384 8080

        # listen.c
        8787

        # nfs
        111 2049 4000 4001 4002 20048
      ];
      allowedUDPPorts = [
        # nfs
        111 2049 4000 4001 4002 20048

        wg0Port
      ];
    };

    nat = {
      enable = true;
      # enableIPv6 = true;

      externalInterface = interface;
      internalInterfaces = [ "wg0" ];
    };

    wg-quick = {
      interfaces = {
        wg0 = {
          address = [ "10.0.0.1/24" /* "fc0:0:0::1/64" */ ];

          listenPort = wg0Port;

          postUp = ''
            ${pkgs.iptables}/bin/iptables -A FORWARD -i wg0 -j ACCEPT
            ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.0.0.1/24 -o ${interface} -j MASQUERADE
            # ${pkgs.iptables}/bin/ip6tables -A FORWARD -i wg0 -j ACCEPT
            # ${pkgs.iptables}/bin/ip6tables -t nat -A POSTROUTING -s fc10:10:10::1/64 -o ${interface} -j MASQUERADE
          '';

          preDown = ''
            ${pkgs.iptables}/bin/iptables -D FORWARD -i wg0 -j ACCEPT
            ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.0.0.1/24 -o ${interface} -j MASQUERADE
            # ${pkgs.iptables}/bin/ip6tables -D FORWARD -i wg0 -j ACCEPT
            # ${pkgs.iptables}/bin/ip6tables -t nat -D POSTROUTING -s fc10:10:10::1/64 -o ${interface} -j MASQUERADE
          '';

          privateKeyFile = config.sops.secrets.wg-vpn.path;

          peers = [
            { # Mason
              publicKey = "l0omfQMKAStQzGCd+Z+eJ3gUzwETEwY5YbNmd8Rrqk4=";
              presharedKeyFile = config.sops.secrets.wg-vpn-shared.path;
              allowedIPs = [ "10.0.0.2/32" /* "fc10:10:10::2/128" */ ];
            }

            { # Lance
              publicKey = "4RxYKRxTz6wZiwKp2xftxz9HSEQbsZxKHSr7rm1sUEE=";
              presharedKeyFile = config.sops.secrets.wg-vpn-shared.path;
              allowedIPs = [ "10.0.0.3/32" /* "fc10:10:10::3/128" */ ];
            }
          ];
        };

        # NOTE interface can only be 15 characters long
        MV-Mighty-Rhino = { # Mullvad
          address = [ "10.64.1.246/32" "fc00:bbbb:bbbb:bb01::1:1f5/128" ];

          privateKeyFile = config.sops.secrets.wg-vpn.path;

          table = "off";

          postUp = ''
            ip route add default dev MV-Mighty-Rhino table 100
            ip rule add from 10.64.1.246 lookup 100
            ip -6 route add default dev MV-Mighty-Rhino table 100
            ip -6 rule add from fc00:bbbb:bbbb:bb01::1:1f5 lookup 100
          '';

          preDown = ''
            ip rule del from 10.64.1.246 lookup 100
            ip route del default dev MV-Mighty-Rhino table 100
            ip -6 rule del from fc00:bbbb:bbbb:bb01::1:1f5 lookup 100
            ip -6 route del default dev MV-Mighty-Rhino table 100
          '';

          peers = [{
            publicKey = "wGqcNxXH7A3bSptHZo7Dfmymy/Y30Ea/Zd47UkyEbzo=";
            allowedIPs = [ "0.0.0.0/0" "::/0" ];
            endpoint = "104.193.135.100:51820"; 
            persistentKeepalive = 25;
          }];
        };
      };
    };
  };

  services = {
    nfs = {
      server = {
        enable = true;
        lockdPort = 4001;
        mountdPort = 4002;
        statdPort = 4000;
        exports = ''
          /mnt/combined 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=7ebed3b5-4558-4371-941a-40e381cc802e)
          /mnt/backup 192.168.0.22/24(insecure,rw,sync,no_subtree_check)
          /mnt/merged 192.168.0.22/24(insecure,rw,sync,no_subtree_check,no_root_squash,fsid=e6373563-e22b-4b5b-87e9-2e8e771acac5)
        '';
      };
    };

    samba = {
      enable = true; 
      openFirewall = true;

      package = pkgs.samba4Full;

      settings = {
        Combined = {
          path = "/mnt/combined";
          "read only" = false;
          browseable = "yes";
          "guest ok" = "yes";
          comment = "Combine all storages";
        };

        Backup = {
          path = "/mnt/backup";
          "read only" = false;
          browseable = "yes";
          "guest ok" = "yes";
          comment = "Raid 1 8tbs";
        };

        Merged = {
          path = "/mnt/merged";
          "read only" = false;
          browseable = "yes";
          "guest ok" = "yes";
          comment = "Random Disks Combined";
        };
      };
    };

    # Auto Discovery For Linux Clients
    avahi = {
      enable = true;
      openFirewall = true;

      nssmdns4 = true;

      publish = {
        enable = true;
        userServices = true;
      };
    };

    # Auto Discovery For Windows Clients
    samba-wsdd = {
      enable = true;
      discovery = true;
      openFirewall = true;
    };
  };
}
