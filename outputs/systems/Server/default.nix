let
  system = "x86_64-linux";
in

args @ {
  root,
  recImportDirectories,
  inputs,
  ...
}:

let
  internal = recImportDirectories root (name: file: path: (import file {
    inherit root recImportDirectories name inputs system;
  }));

  nixpkgs = inputs.nixpkgs;

  pkgs = import nixpkgs {
    inherit system;

    config.allowUnfree = true;

    overlays = [
      (final: prev: { local = internal.pkgs; lib = prev.lib // { local = internal.lib; }; })
    ];
  };

  nixosSystem = nixpkgs.lib.nixosSystem {
    inherit system pkgs;

    specialArgs = args // {
      inherit system;
      modules = internal.modules;
    };

    modules = with internal.modules.nixos; [
      external.home-manager
      external.disko
      external.sops-nix
      external.impermanence
      external.nix-topology
    ] ++ [
      # Directories
      ./homes
      ./secrets

      # Files
      ./config.nix
      ./hardware.nix
      ./network.nix
      ./impermanence.nix
      ./git.nix
      ./storage.nix
    ];
  };
in {
  inherit nixosSystem;

  listen = import ./listen.nix { inherit system pkgs; };
} // (pkgs.lib.local.nixos-build pkgs nixosSystem)
