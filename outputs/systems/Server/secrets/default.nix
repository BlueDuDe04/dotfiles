{
  config,
  ...
}:

{
  sops = {
    age.keyFile = "/persist/system/var/lib/sops-nix/age/keys.txt";

    secrets = {
      root = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      server = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      gameyfinProps = {
        sopsFile = ./gameyfin.properties;
        format = "binary";

        owner = config.users.users.server.name;
        group = config.users.users.server.group;

        path = "/mnt/backup/gameyfin/gameyfin.properties";
      };

      wg-vpn = {
        sopsFile = ./wg-vpn;

        format = "binary";
      };

      transmission = {
        sopsFile = ./transmission-settings;

        format = "binary";
      };

      wg-vpn-shared = {
        sopsFile = ./wg-vpn-shared;

        format = "binary";
      };
    };
  };
}
