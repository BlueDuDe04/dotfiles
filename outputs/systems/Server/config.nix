{
  inputs,
  system,
  pkgs,
  lib,
  config,
  ...
}:

{
  nix = {
    gc = {
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 7d";
    };

    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  # Set your time zone.
  time.timeZone = "America/Vancouver";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  documentation.nixos.enable = false;

  # Do not change unless you know what you are doing!
  system.stateVersion = "22.11"; # Did you read the comment?

  services = {
    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
    };

    syncthing = {
      enable = true;

      user = "server";
      group = "users";

      dataDir = "/mnt/backup/syncthing/Sync";
      configDir = "/mnt/backup/syncthing/config";

      guiAddress = "192.168.0.22:8384";
      openDefaultPorts = true;
    };

    jellyfin = {
      enable = true;
      openFirewall = true;

      user = "server";
      group = "users";

      dataDir = "/mnt/backup/jellyfin/dataDir";
      cacheDir = "/mnt/backup/jellyfin/cacheDir";
    };

    transmission = {
      enable = true;
      openFirewall = true;
      openPeerPorts = true;
      openRPCPort = true;

      user = "server";
      group = "users";

      home = "/mnt/combined/transmission";

      credentialsFile = config.sops.secrets.transmission.path;

      settings = {
        bind-address-ipv4 = "10.64.1.246";
        bind-address-ipv6 = "fc00:bbbb:bbbb:bb01::1:1f5";

        rpc-username = "server";
        rpc-authentication-required = true;

        rpc-bind-address = "192.168.0.22";
        rpc-port = 9091;

        rpc-whitelist-enabled = false;

        download-dir = "/mnt/combined/transmission/downloads";

        watch-dir-enabled = true;
        watch-dir = "/mnt/combined/transmission/torrents";

        incomplete-dir-enabled = true;
        incomplete-dir = "/mnt/combined/transmission/incomplete";
      };
    };
  };

  systemd.services = {
    gameyfin = {
      serviceConfig = {
        WorkingDirectory = "/mnt/backup/gameyfin";
        ExecStart = "${pkgs.jdk21}/bin/java -jar ${inputs.gameyfin}";

        User = "server";
        Group = "users";
      };

      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
    };

    listen = {
      serviceConfig = {
        WorkingDirectory = "/persist/system/home/server/Persist";
        ExecStart = "${import ./listen.nix { inherit system pkgs; }}/bin/listen";

        User = "server";
        Group = "users";
      };

      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
    };
  };

  environment = {
    defaultPackages = lib.mkForce [];

    systemPackages = with pkgs; [
      fish
    ];
  };
}
