{
  pkgs,
  config,
  ...
}:

{
  # Create a new project:
  # > sudo -u git bash -c "git init -b branch --bare ~/myproject.git"
  # https://nixos.wiki/wiki/Git

  users = {
    groups.git = {};

    users.git = {
      isSystemUser = true;
      group = "git";
      home = "/mnt/combined/git";
      createHome = true;
      shell = "${pkgs.git}/bin/git-shell";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFSRSgJPxEgxMDLE1BrwKiz/QF+b8ywAQbkaG5cAp79J Bennettmason04@gmail.com"
      ];
      hashedPasswordFile = config.sops.secrets.server.path;
    };
  };

  services.openssh = {
    enable = true;
    extraConfig = ''
      Match user git
        AllowTcpForwarding no
        AllowAgentForwarding no
        PasswordAuthentication no
        PermitTTY no
        X11Forwarding no
    '';
  };

  environment.systemPackages = with pkgs; [
    git
  ];
}
