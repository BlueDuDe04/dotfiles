{
  system,
  pkgs,
  config,
  ...
}:

{
  boot = {
    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "megaraid_sas" "sd_mod" ];
    };

    kernelModules = [ "kvm-intel" ];

    kernelParams = [ "i915.enable_guc=2" ];

    extraModulePackages = [ ];

    loader.grub = {
      enable = true;
      efiSupport = true;
      efiInstallAsRemovable = true;
    };

    swraid = {
      enable = true;
      mdadmConf = ''
        ARRAY /dev/md0 metadata=1.2 UUID=d34c4f85:a61f39e9:89774f4c:1b2b3b34
      '';
    };
  };

  hardware = {
    enableRedistributableFirmware = true;
    cpu.intel.updateMicrocode = config.hardware.enableRedistributableFirmware;

    graphics = {
      enable = true;

      extraPackages = with pkgs; [
        intel-media-driver
        libvdpau-va-gl
        vpl-gpu-rt
      ];
    };
  };

  environment = {
    sessionVariables = {
      LIBVA_DRIVER_NAME = "iHD";
    };

    systemPackages = with pkgs; [
      disko
      intel-gpu-tools
      libva-utils
      mergerfs
    ];
  };

  nixpkgs.hostPlatform = system;
  powerManagement.cpuFreqGovernor = "performance";

  fileSystems."/persist".neededForBoot = true;

  disko.devices = {
    disk = {
      main = {
        device = "/dev/disk/by-id/wwn-0x5000c50044acde48";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              size = "1M";
              type = "EF02"; # for grub MBR
            };

            ESP = {
              type = "EF00";
              size = "500M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };

            persist = {
              size = "100G";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/persist";
                mountOptions = [
                  "noexec"
                ];
              };
            };

            nix = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/nix";
              };
            };
          };
        };
      };
    };

    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = [
          "size=25%"
          "noexec"
        ];
      };
    };
  };
}
