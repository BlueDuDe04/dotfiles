{
  inputs,
  pkgs,
  ...
}:

let
  monado-settings = {
    WMR_HANDTRACKING = "0";
    VIT_SYSTEM_LIBRARY_PATH = "${pkgs.local.basalt-monado}/lib/libbasalt.so";
  };
in

{
  services.monado = {
    enable = true;
    defaultRuntime = true;
    highPriority = true;

    package = pkgs.monado.overrideAttrs (final: prev: {
      src = inputs.monado-controller-tracking;
    });
  };

  systemd.user.services.monado.environment = monado-settings;

  environment.variables = monado-settings;
}
