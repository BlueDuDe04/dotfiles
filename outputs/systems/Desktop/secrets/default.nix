{
  ...
}:

{
  sops = {
    age.keyFile = "/persist/var/lib/sops-nix/age/keys.txt";

    secrets.root = {
      sopsFile = ./passwords.yaml;
      neededForUsers = true;
    };

    secrets.mason = {
      sopsFile = ./passwords.yaml;
      neededForUsers = true;
    };

    secrets.work = {
      sopsFile = ./passwords.yaml;
      neededForUsers = true;
    };

    secrets.share = {
      sopsFile = ./smb-secrets;
      format = "binary";
    };
  };
}
