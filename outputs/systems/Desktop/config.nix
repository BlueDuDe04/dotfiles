{
  root,
  pkgs,
  lib,
  ...
}:

let
  font = pkgs.nerdfonts.override { fonts = [ "VictorMono" "FiraCode" ]; };
in

{
  boot.binfmt.emulatedSystems = [
    "x86_64-windows"
    "aarch64-linux"
    "riscv64-linux"
  ];

  security = {
    rtkit.enable = true;
    polkit.enable = true;
  };

  xdg = {
    portal = {
      enable = true;

      xdgOpenUsePortal = true;
      config.common.default = "*";

      # gtk portal needed to make gtk apps happy
      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
        xdg-desktop-portal-hyprland
      ];
    };
  };

  services = {
    greetd = {
      enable = true;
      settings = {
        initial_session = {
          command = "Hyprland";
          user = "mason";
        };

        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd Hyprland";
          user = "greeter";
        };
      };
    };

    kmonad = {
      enable = true;
      keyboards = {
        keyboard = {
          device = "/dev/input/by-id/usb-LiteOn_Lenovo_Essential_Wired_Combo-event-kbd";
          config = builtins.readFile (root + "/extras/kmonad/colemak-dh-wide.kbd");
        };
      };
    };

    pipewire = {
      enable = true;
      audio.enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;
    };

    mullvad-vpn.enable = true;

    printing.enable = true;
    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
  };

  stylix = {
    enable = true;
    autoEnable = true;

    homeManagerIntegration = {
      autoImport = true;
      followSystem = true;
    };

    image =  root + "/extras/wallpapers/Anime-Girl-Blue-Sky.png";
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";

    opacity.terminal = 0.9;

    cursor = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
      size = 26;
    };

    fonts = let
      settings = {
        package = font;
        name = "Firacode Nerd Font Mono:style=Bold";
      };
    in {
      serif = settings;
      sansSerif = settings;
      monospace = settings;
    };
  };

  fonts.packages = [ font ];

  environment = {
    sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };

    defaultPackages = lib.mkForce [];

    systemPackages = with pkgs; [
      xdg-utils
      mullvad-vpn

      local.nv

      hyprpicker

      slurp
      wtype
      wl-clipboard
      cliphist

      pavucontrol
      mpv

      # CLI
      git
      wget
      zip
      unzip

      gojq
      ripgrep
      fd

      # TUI
      htop
    ];
  };
}
