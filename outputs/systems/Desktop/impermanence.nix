{
  ...
}:

{
  fileSystems."/persist".neededForBoot = true;
  environment.persistence."/persist" = {
    enable = true;
    hideMounts = true;

    directories = [
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"

      # Nix
      "/root/.cache/nix"
      "/root/.local/share/nix"
    ];

    files = [
      "/var/lib/sops-nix/age/keys.txt"

      # Sudo
      "/var/db/sudo/lectured/1000"
    ];
  };
}
