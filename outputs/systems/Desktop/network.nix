{
  name,
  pkgs,
  config,
  ...
}:

let
  sambaHost = "192.168.0.22";
  automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
  permissions = "file_mode=0660,dir_mode=0770";
in 

{
  networking = {
    hostName = name;

    networkmanager.enable = true;
  };

  users.groups = {
    share = {
      gid = 4321;
      members = [ "mason" ];
    };
  };

  users.users.mason.extraGroups = [ "share" ];

  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = [ pkgs.cifs-utils ];

  fileSystems."/mnt/share/server/combined" = {
    device = "//${sambaHost}/Combined";
    fsType = "cifs";
    options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
  };

  fileSystems."/mnt/share/server/backup" = {
    device = "//${sambaHost}/Backup";
    fsType = "cifs";
    options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
  };

  fileSystems."/mnt/share/server/merged" = {
    device = "//${sambaHost}/Merged";
    fsType = "cifs";
    options = ["${automount_opts},credentials=${config.sops.secrets.share.path},gid=${toString config.users.groups.share.gid},${permissions}"];
  };
}
