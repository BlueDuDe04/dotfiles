{
  DP-1 ? null,
  DP-2 ? null,
  HDMI-A-1 ? null,
}:

let
  ifNull = w: if w != null then w else null;
in

[

{
  name = "DP-1";
  wallpaper = ifNull DP-1;
  primary = true;
  width = 2560;
  height = 1440;
  workspaces = [
    "4"
    "5"
    "6"
    "7"
  ];
}

{
  name = "DP-2";
  wallpaper = ifNull DP-2;
  width = 2560;
  height = 1440;
  x = -1440;
  y = -580;
  transform = "1";
  workspaces = [
    "1"
    "2"
    "3"
  ];
}

{
  name = "HDMI-A-1";
  wallpaper = ifNull HDMI-A-1;
  width = 2560;
  height = 1440;
  x = 2560;
  y = -495;
  transform = "3";
  workspaces = [
    "8"
    "9"
    "0"
  ];
}

]
