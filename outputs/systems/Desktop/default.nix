let
  system = "x86_64-linux";
in

args @ {
  root,
  recImportDirectories,
  inputs,
  ...
}:

let
  internal = recImportDirectories root (name: file: path: (import file {
    inherit root recImportDirectories name inputs system;
  }));

  nixpkgs = inputs.nixpkgs;

  pkgs = import nixpkgs {
    inherit system;

    config.allowUnfree = true;

    overlays = [
      (final: prev: { local = internal.pkgs; lib = prev.lib // { local = internal.lib; }; })
    ];
  };

  nixosSystem = nixpkgs.lib.nixosSystem {
    inherit system pkgs;

    specialArgs = args // {
      inherit system;
      modules = internal.modules;
    };

    modules = with internal.modules.nixos; [
      external.sops-nix
      external.home-manager
      external.stylix
      external.disko
      external.impermanence
      external.nix-topology

      setup
      nvidia
    ] ++ [
      # Directories
      ./homes
      ./secrets

      # Files
      ./config.nix
      ./hardware.nix
      ./impermanence.nix
      ./network.nix
      # ./vr.nix
    ];
  };
in (internal.lib.nixos-build pkgs nixosSystem) // {
  inherit nixosSystem;
}

