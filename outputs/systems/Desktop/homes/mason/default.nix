{
  config,
  ...
}:

{
  users.users.mason = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" ];
    hashedPasswordFile = config.sops.secrets.mason.path;
  };

  home-manager.users.mason = ./home.nix;

  environment.persistence."/persist".users.mason = {
    directories = [
      "Downloads"
      "Documents"
      "Projects"
      "Pictures"
      "Videos"
      "Games"
      "Sync"

      { directory = ".ssh"; mode = "0700"; }

      # Nix
      ".cache/nix"
      ".local/share/nix"

      # Direnv
      ".local/share/direnv"

      # Syncthing
      ".local/state/syncthing"

      # Firefox
      ".mozilla"
      ".cache/mozilla"

      # Neovim
      ".cache/nvim"
      ".local/share/nvim"
      ".local/state/nvim"

      # Go
      ".cache/go"

      # Vesktop
      ".config/vesktop"

      # Steam
      ".steam"
      ".local/share/Steam"
      ".local/share/vulkan"

      # Zoxide
      ".local/share/zoxide"

      # FreeTube
      ".config/FreeTube"
    ];

    files = [
      # Sops Keys
      ".config/sops/age/keys.txt"

      # Fish
      ".local/share/fish/fish_history"

      # SteamVR
      # ".config/openvr/openvrpaths.vrpath"
    ];
  };
}
