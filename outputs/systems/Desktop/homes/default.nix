{
  root,
  inputs,
  system,
  pkgs,
  modules,
  config,
  ...
}:

{
  imports = [
    ./mason
  ];

  users.users.root.hashedPasswordFile = config.sops.secrets.root.path;

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    extraSpecialArgs = {
      inherit root inputs system pkgs modules;
    };

    sharedModules = with modules.home; [
      utils
    ];
  };
}
