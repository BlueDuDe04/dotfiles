{
  lib,
  config,
  ...
}:

{
  security.sudo.enable = true;
  security.doas.enable = true;

  # nix.settings.allowed-users = [ "root" ];

  users = {
    mutableUsers = false;

    users = {
      root.hashedPasswordFile = config.sops.secrets.root.path;

      mason = {
        isNormalUser = true;
        extraGroups = [ "wheel" ];
        hashedPasswordFile = config.sops.secrets.mason.path;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFSRSgJPxEgxMDLE1BrwKiz/QF+b8ywAQbkaG5cAp79J Bennettmason04@gmail.com"
        ];
      };
    };
  };

  services.openssh = {
    enable = true;
    settings = {
    };
  };

  environment.defaultPackages = lib.mkForce [ ];
}
