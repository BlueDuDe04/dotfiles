{
  name,
  pkgs,
  config,
  ...
}:

let
  interface = "enp1s0";
  wg0Port = 51820;
in

{
  networking = {
    hostName = name;

    useDHCP = true;

    defaultGateway = {
      inherit interface;
      address = "192.168.0.1";
    };

    # defaultGateway6 = {
    #   inherit interface;
    #   address = "fe80::9e1e:95ff:fe8c:9320";
    # };

    nameservers = [
      "192.168.0.1"
    ];

    interfaces.${interface} = {
      ipv4.addresses = [{
        address = "192.168.0.49";
        prefixLength = 24;
      }];

      # ipv6.addresses = [{
      #   address = "2001:569:7dbf:f300:926e:818:62d:2759";
      #   prefixLength = 64;
      # }];
    };

    nat = {
      enable = true;
      # enableIPv6 = true;
      
      externalInterface = interface;
      internalInterfaces = [ "wg0" ];
    };

    firewall = {
      enable = true;

      allowedUDPPorts = [ wg0Port ];
    };

    wg-quick = {
      interfaces = {
        wg0 = {
          address = [ "10.0.0.1/24" /* "fc10:10:10::1/64" */ ];

          listenPort = wg0Port;

          postUp = ''
            ${pkgs.iptables}/bin/iptables -A FORWARD -i wg0 -j ACCEPT
            ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.0.0.1/24 -o ${interface} -j MASQUERADE
            # ${pkgs.iptables}/bin/ip6tables -A FORWARD -i wg0 -j ACCEPT
            # ${pkgs.iptables}/bin/ip6tables -t nat -A POSTROUTING -s fc10:10:10::1/64 -o ${interface} -j MASQUERADE
          '';

          preDown = ''
            ${pkgs.iptables}/bin/iptables -D FORWARD -i wg0 -j ACCEPT
            ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.0.0.1/24 -o ${interface} -j MASQUERADE
            # ${pkgs.iptables}/bin/ip6tables -D FORWARD -i wg0 -j ACCEPT
            # ${pkgs.iptables}/bin/ip6tables -t nat -D POSTROUTING -s fc10:10:10::1/64 -o ${interface} -j MASQUERADE
          '';

          privateKeyFile = config.sops.secrets.wg-vpn.path;

          peers = [
            { # Mason
              publicKey = "l0omfQMKAStQzGCd+Z+eJ3gUzwETEwY5YbNmd8Rrqk4=";
              presharedKeyFile = config.sops.secrets.wg-vpn-shared.path;
              allowedIPs = [ "10.0.0.2/32" /* "fc10:10:10::2/128" */ ];
            }

            { # Lance
              publicKey = "4RxYKRxTz6wZiwKp2xftxz9HSEQbsZxKHSr7rm1sUEE=";
              presharedKeyFile = config.sops.secrets.wg-vpn-shared.path;
              allowedIPs = [ "10.0.0.3/32" /* "fc10:10:10::3/128" */ ];
            }
          ];
        };
      };
    };
  };
}
