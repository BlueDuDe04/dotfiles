{
  ...
}:

{
  sops = {
    age.keyFile = "/persist/system/var/lib/sops-nix/age/keys.txt";

    secrets = {
      root = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      mason = {
        sopsFile = ./passwords.yaml;
        neededForUsers = true;
      };

      wg-vpn = {
        sopsFile = ./wg-vpn;

        format = "binary";
      };

      wg-vpn-shared = {
        sopsFile = ./wg-vpn-shared;

        format = "binary";
      };
    };
  };
}
