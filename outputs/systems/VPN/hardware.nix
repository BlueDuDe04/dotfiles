{
  system,
  config,
  ...
}:

{
  boot = {
    initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" ];

    kernelModules = [ "kvm-intel" ];

    loader.grub.enable = true;
  };

  hardware = {
    enableRedistributableFirmware = true;
    cpu.intel.updateMicrocode = config.hardware.enableRedistributableFirmware;
  };

  nixpkgs.hostPlatform = system;
  powerManagement.cpuFreqGovernor = "performance";

  fileSystems."/persist".neededForBoot = true;

  disko.devices = {
    disk = {
      main = {
        device = "/dev/disk/by-id/ata-Hoodisk_SSD_JCTTC7A11227856";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              size = "1M";
              type = "EF02"; # for grub MBR
            };

            ESP = {
              type = "EF00";
              size = "500M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };

            persist = {
              size = "2G";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/persist";
                # mountOptions = [
                #   "noexec"
                # ];
              };
            };

            nix = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/nix";
              };
            };
          };
        };
      };
    };

    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = [
          "size=25%"
          # "noexec"
        ];
      };
    };
  };
}
