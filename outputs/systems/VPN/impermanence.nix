{
  ...
}:

{
  environment.persistence."/persist/system" = {
    enable = true;
    hideMounts = true;

    directories = [
      "/var/log"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"

      # Ssh
      "/etc/ssh"
    ];

    files = [
      "/etc/machine-id"

      "/var/lib/sops-nix/age/keys.txt"

      # Sudo
      "/var/db/sudo/lectured/1000"
    ];
  };
}
