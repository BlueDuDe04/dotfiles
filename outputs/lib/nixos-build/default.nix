{
  name,
  ...
}:

pkgs: nixosSystem:

let
  os = nixosSystem.config.system.build.toplevel;
in

pkgs.writeScriptBin name /* sh */ ''
  #!/bin/sh

  echo "${os}"

  ${os}/bin/switch-to-configuration check

  nix-env -p "/nix/var/nix/profiles/system" --set "${os}"

  ${os}/bin/switch-to-configuration "$@"
''
