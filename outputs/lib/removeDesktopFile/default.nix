{
  ...
}:

{
  removeDesktopFile = pkgs: { name, pkg }: pkgs.stdenv.mkDerivation {
    name = name;
    src = pkg;
    installPhase = ''
      mkdir -p $out/share
      for item in $src/*; do
          if [ ! "$item" == "$src/share" ]; then
              ln -s $src/$(basename $item) $out/$(basename $item)
          fi
      done
      for item in $src/share/*; do
          if [ ! "$item" == "$src/share/applications" ]; then
              ln -s $src/share/$(basename $item) $out/share/$(basename $item)
          fi
      done
    '';
  };
}
