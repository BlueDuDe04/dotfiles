{
  ...
}:

pkgs: {
  package ? pkgs.nwg-drawer,
  args ? [],
  drawer ? "",
}:

let
  inherit (builtins) toFile concatStringsSep;
  inherit (pkgs) stdenv writeShellScriptBin;
in 

stdenv.mkDerivation {
  name = package.name;

  src = package;
  bin = writeShellScriptBin "nwg-drawer" ''
    ${package}/bin/nwg-drawer -s ${toFile "drawer.css" drawer} ${concatStringsSep " " args}
  '';

  installPhase = ''
    mkdir -p $out

    for item in $src/*; do
      if [ ! "$item" == "$src/bin" ]; then
        ln -s $src/$(basename $item) $out/$(basename $item)
      fi
    done

    ln -s $bin/bin $out/bin
  '';
}
