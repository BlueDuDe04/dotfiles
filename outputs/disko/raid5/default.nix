let
  raid = device: raid: {
    inherit device;
    type = "disk";
    content = {
      type = "gpt";
      partitions = {
        mdadm = {
          size = "100%";
          content = {
            type = "mdraid";
            name = raid;
          };
        };
      };
    };
  };
in

{
  disko.devices = {
    disk = {
      disk1 = raid "/dev/disk/by-id/wwn-0x5000cca252e695de" "raid5";
      disk2 = raid "/dev/disk/by-id/wwn-0x5000cca252e6f709" "raid5";
      disk3 = raid "/dev/disk/by-id/wwn-0x5000cca24ce94d1a" "raid0";
      disk4 = raid "/dev/disk/by-id/wwn-0x5000c50087bdbb05" "raid0";
    };

    mdadm = {
      raid0 = {
        type = "mdadm";
        level = 0;
        content = {
          type = "mdraid";
          name = "raid5";
        };
      };

      raid5 = {
        type = "mdadm";
        level = 5;
        content = {
          type = "filesystem";
          format = "ext4";
        };
      };
    };
  };
}
