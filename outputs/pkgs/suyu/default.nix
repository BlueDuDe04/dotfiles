{
  name,
  inputs,
  nixpkgs,
  lib,
  ...
}:

let
  systems = [
    "x86_64-linux"
    "aarch64-linux"
    "i686-linux"
  ];

  inherit (lib) genAttrs;
  forAllSystems = genAttrs systems;
in

forAllSystems (system: let
  pkgs = import nixpkgs { inherit system; };

  suyu = pkgs.appimageTools.wrapType2 {
    pname = name;
    version = "v0.0.3";

    src = inputs.suyu;
  };

  desktopItem = ''
    [Desktop Entry]
    Exec=${name}
    GenericName=${name}
    Icon=${name}
    Keywords=live;tv
    Name=${name}
    StartupWMClass=${name}
    Type=Application
    Version=0.0.3
    Comment=Nintendo Switch Emulator
  '';
in { 
  ${name} = pkgs.stdenv.mkDerivation {
    inherit name;

    src = pkgs.writeShellScriptBin name ''${suyu}/bin/suyu -ql "$@"'';

    installPhase = ''
      mkdir -p $out/share/icons/hicolor/256x256/apps
      mkdir -p $out/share/applications

      ln -s $src/bin $out/bin
      echo "${desktopItem}" > $out/share/applications/${name}.desktop
      ln -s ${./icon.png} $out/share/icons/hicolor/256x256/apps/${name}.png
    '';
  };
})

