let
  systems = [
    "aarch64-linux"
    "i686-linux"
    "x86_64-linux"
    "x86_64-darwin"
    "aarch64-darwin"
  ];
in

args @ {
  name,
  inputs,
  ...
}:

let
  forAllSystems = inputs.nixpkgs.lib.genAttrs systems;

  pkg = system: let
    pkgs = import inputs.nixpkgs { inherit system; };
  in pkgs.writeShellScriptBin name ''
    # Function to check if the first argument contains '#'
    has_hashtag() {
      echo "$1" | grep -q '#'
    }

    # Check if at least one argument is provided
    if [ $# -lt 1 ]; then
        echo "Usage: $0 [pkgs]"
        exit 1
    fi

    pkgs=()

    # Iterate over each argument
    for arg in "$@"; do
      # Check if the argument contains '#'
      if has_hashtag "$arg"; then
        pkgs+=("$arg")
      else
        pkgs+=("nixpkgs#$arg")
      fi
    done

    nix build "''${pkgs[@]}" --no-link --print-out-paths
  '';
in

if builtins.hasAttr "system" args then
    pkg args.system
else

forAllSystems pkg
