let
  systems = [
    "aarch64-linux"
    "i686-linux"
    "x86_64-linux"
    "x86_64-darwin"
    "aarch64-darwin"
  ];
in

args @ {
  name,
  inputs,
  ...
}:

let
  forAllSystems = inputs.nixpkgs.lib.genAttrs systems;

  pkg = system: let
    pkgs = import inputs.nixpkgs { inherit system; };
  in pkgs.writeShellScriptBin name ''
    # Function to check if the first argument contains '#'
    has_hashtag() {
      echo "$1" | grep -q '#'
    }

    # Check if at least one argument is provided
    if [ $# -lt 1 ]; then
        echo "Usage: $0 <pkg_name> [additional_arguments]"
        exit 1
    fi

    # Extract the first argument
    # Check if the first argument contains '#'
    if has_hashtag "$1"; then
        pkg_name=$1
    else
        pkg_name=nixpkgs#$1
    fi

    # Remove the first argument from the list
    shift

    nix run $pkg_name -- "$@"
  '';
in

if builtins.hasAttr "system" args then
    pkg args.system
else

forAllSystems pkg
