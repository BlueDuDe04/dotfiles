args @ {
  name,
  inputs,
  ...
}:

let
  inherit (builtins) replaceStrings;

  systems = [ "x86_64-linux" ];

  forAllSystems = inputs.nixpkgs.lib.genAttrs systems;

  pkg = system: let
    pkgs = import inputs.nixpkgs {
      inherit system;

      config.allowUnfree = true;
    };
  in pkgs.plex-desktop.override {
    fetchurl = _: inputs.plex-htpc;
    buildFHSEnv = config: pkgs.buildFHSEnv (config // {
      pname = name;
      version = "1.96.0";
      extraInstallCommands = replaceStrings [
        "plex-desktop.desktop"
        "Exec=plex-desktop"
      ] [
        "plex-htpc.desktop"
        "Exec=plex-htpc"
      ] config.extraInstallCommands;
    });
  };
in

if builtins.hasAttr "system" args then
  pkg args.system
else

forAllSystems pkg
