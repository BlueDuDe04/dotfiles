local wk = require('which-key')

local telescope = require("telescope.builtin")
local harpoon = require("harpoon")
local neotest = require("neotest")
local trouble = require("trouble")
local otter = require('otter')
-- local dap = require("dap")
-- local dap_virtual_text = require("nvim-dap-virtual-text")

-- Harpoon
local function toggle_telescope(harpoon_files)
  local conf = require("telescope.config").values
  local file_paths = {}
  for _, item in ipairs(harpoon_files.items) do
    table.insert(file_paths, item.value)
  end

  require("telescope.pickers").new({}, {
    prompt_title = "Harpoon",
    finder = require("telescope.finders").new_table({
      results = file_paths,
    }),
    previewer = conf.file_previewer({}),
    sorter = conf.generic_sorter({}),
  }):find()
end

-- Neorg
local buf = nil
local store_non_norg_buf = function()
  if vim.filetype.match({ buf = 0 }) ~= "norg" then
    buf = vim.fn.expand("%:p")
  end
end

-- Debugger
-- vim.keymap.set('n', '<C-n>', function() dap.step_over() end)
-- vim.keymap.set('n', '<C-p>', function() dap.step_back() end)

local function show_all_hovers()
  local buf_clients = vim.lsp.get_active_clients({ bufnr = 0 })
  local params = vim.lsp.util.make_position_params()

  local collect = {}
  local count = 0

  for _, client in ipairs(buf_clients) do
    if client.server_capabilities.hoverProvider then
      count = count + 1
      client.request('textDocument/hover', params, function(_, result)
        if result and result.contents then
          table.insert(collect, "## " .. client.name)
          table.insert(collect, "---")
          vim.list_extend(collect, vim.lsp.util.convert_input_to_markdown_lines(result.contents));
          if count > 1 then
            table.insert(collect, "---")
          end
        end

        count = count - 1

        -- If all requests have been processed, display the hover results
        if count == 0 then
          vim.lsp.util.open_floating_preview(
            collect,
            'markdown',
            { border = 'rounded' } -- Updated: Removed unsupported keys
          )
        end
      end)
    end
  end
end

-- LSP Mappings
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
    vim.keymap.del('n', 'K', { buffer = ev.buf })

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local lspBuf = vim.lsp.buf
    local opts = { buffer = ev.buf }

    local maps = {
      l = {
        name = "LSP",
        l = { function() show_all_hovers() end, "LSP Hover", opts },
        h = { lspBuf.signature_help, "LSP Signature Help", opts },
        f = { function() lspBuf.format { async = true } end, "Format", opts },
        d = { lspBuf.definition, "Definition", opts },
        D = { lspBuf.declaration, "Declaration", opts },
        t = { lspBuf.type_definition, "Definition", opts },
        r = { lspBuf.references, "References", opts },
        n = { lspBuf.rename, "Rename", opts },
        c = { lspBuf.code_action, "Code Action", opts },
        i = { lspBuf.implementation, "Implementation", opts },

        w = {
          name = "Workspaces",
          a = { lspBuf.add_workspace_folder, "Add Folder", opts },
          r = { lspBuf.remove_workspace_folder, "Remove Folder", opts },
          l = { function() print(vim.inspect(lspBuf.list_workspace_folders())) end, "List Folders", opts },
        },
      },
    }

    wk.register(maps, {
      mode = "n",
      prefix = "<leader>"
    })
  end,
})

-- Normal Mode
wk.register({
  l = {
    e = { function() otter.activate() end, "Enable Embeded LSPs" },
    x = { function() otter.deactivate() end, "Remove Embeded LSPs" },
  },

  f = {
    name = "Find",
    f = { telescope.find_files, "Files" },
    g = { telescope.git_files, "Git Files" },
    b = { telescope.buffers, "Buffers" },
    h = { telescope.help_tags, "Help" },
    w = { telescope.grep_string, "Current Word" },
    G = { telescope.live_grep, "By Grep" },
    d = { telescope.diagnostics, "Diagnostics" },
    r = { telescope.resume, "Resume" },
    j = { telescope.jumplist, "Previous Jumps" },
  },

  t = {
    name = "Test",
    t = { function() neotest.run.run() end, "Run" },
    a = { function() neotest.run.run(vim.fn.expand('%')) end, "Run All" },

    s = { function() neotest.summary.toggle() end, "Summary Toggle" },

    o = { function() neotest.output.open() end, "Output Open" },
    O = { function() neotest.output_panel.toggle() end, "Output Panel Toggle" },

    n = { function() neotest.jump.next() end, "Jump Next" },
    p = { function() neotest.jump.prev() end, "Jump Prev" },
  },

  w = {
    name = "Trouble",
    e = { function() trouble.close() end, "Exit" },
    w = { function() trouble.open("workspace_diagnostics") end, "workspace_diagnostics" },
    d = { function() trouble.open("document_diagnostics") end, "document_diagnostics" },
    r = { function() trouble.open("lsp_references") end, "lsp_references" },
  },

  -- d = {
  --   name = "Debugger",
  --   d = { function()
  --     dap.continue()
  --     dap_virtual_text.enable()
  --   end, "Start" },
  --   e = { function()
  --     dap.terminate()
  --     dap_virtual_text.disable()
  --   end, "End" },
  --   s = { function() dap.toggle_breakpoint() end, "Toggle Breakpoints" },
  --   c = { function() dap.clear_breakpoints() end, "Clear Breakpoints" },
  --   i = { function() dap.step_into() end, "Step Into" },
  -- },

  a = {
    name = "Assemble",
    d = { function() vim.cmd "Disassemble" end, "Disassemble" },
    f = { function() vim.cmd "DisassembleFull" end, "DisassembleFull" },
    c = { function() vim.cmd "DisassembleConfig" end, "DisassembleConfig" },
    s = { function() vim.cmd "DisassembleSaveConfig" end, "DisassembleSaveConfig" },
  },

  n = {
    name = "Notes",
    n = {
      function()
        store_non_norg_buf()
        vim.cmd "Neorg workspace notes"
      end,
      "Neorg Notes"
    },
    t = {
      function()
        store_non_norg_buf()
        vim.cmd "Neorg journal today"
      end,
      "Neorg Journal Today"
    },
    T = {
      function()
        store_non_norg_buf()
        vim.cmd "Neorg journal tomorrow"
      end,
      "Neorg Journal Tomorrow"
    },
    y = {
      function()
        store_non_norg_buf()
        vim.cmd "Neorg journal yesterday"
      end,
      "Neorg Journal Yesterday"
    },
    e = {
      function()
        if buf ~= nil and buf ~= "" then vim.cmd.e(buf) end
      end,
      "Exit Notes"
    },
  },

  e = {
    name = "Folds",
    f = { "za", "Toggle Fold Under Cursor" },
    a = { "zA", "Toggle All Folds Under Cursor" },
    o = { "zR", "Open All Folds" },
    c = { "zM", "Close All Folds" },
  },

  u = { vim.cmd.UndotreeToggle, "Undotree Toggle" },

  -- ClipBoard
  p = {
    name = "Clipboard",
    ["2"] = { function() vim.cmd.normal("a" .. vim.fn.system("cliphist list | sed -n 2p | cliphist decode")) end, "Cliphist Paste Entry 2" },
    ["3"] = { function() vim.cmd.normal("a" .. vim.fn.system("cliphist list | sed -n 3p | cliphist decode")) end, "Cliphist Paste Entry 3" },
    ["4"] = { function() vim.cmd.normal("a" .. vim.fn.system("cliphist list | sed -n 4p | cliphist decode")) end, "Cliphist Paste Entry 4" },
    ["5"] = { function() vim.cmd.normal("a" .. vim.fn.system("cliphist list | sed -n 5p | cliphist decode")) end, "Cliphist Paste Entry 5" },
  },
}, {
  mode = "n",
  prefix = "<leader>"
})

wk.register({
  -- Harpoon
  ["<C-h>"] = {
    function()
      harpoon:list():add()
      harpoon:sync()
    end,
    "Append"
  },
  ["<C-l>"] = {
    function()
      harpoon.ui:toggle_quick_menu(harpoon:list())
      harpoon:sync()
    end,
    "Toggle Quick Menu"
  },

  ["<C-0>"] = { function() toggle_telescope(harpoon:list()) end, "Harpoon Search" },
  ["<C-1>"] = { function() harpoon:list():select(1) end, "Harpoon 1" },
  ["<C-2>"] = { function() harpoon:list():select(2) end, "Harpoon 2" },
  ["<C-3>"] = { function() harpoon:list():select(3) end, "Harpoon 3" },
  ["<C-4>"] = { function() harpoon:list():select(4) end, "Harpoon 4" },
  ["<C-5>"] = { function() harpoon:list():select(5) end, "Harpoon 5" },
  ["<C-6>"] = { function() harpoon:list():select(6) end, "Harpoon 6" },
  ["<C-7>"] = { function() harpoon:list():select(7) end, "Harpoon 7" },
  ["<C-8>"] = { function() harpoon:list():select(8) end, "Harpoon 8" },
  ["<C-9>"] = { function() harpoon:list():select(9) end, "Harpoon 9" },
}, {
  mode = { "n", "i", "x", },
})
