require('neotest').setup({
  -- Loging for Zig
  -- log_level = vim.log.levels.TRACE,
  -- :exe 'edit' stdpath('log').'/neotest-zig.log'
  ...,

  adapters = {
    require("neotest-zig")({
      dap = {
        adapter = "lldb",
      }
    }),
  }
})
