local lspconfig = require('lspconfig')

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true
}
local language_servers = require("lspconfig").util.available_servers() -- or list servers manually like {'gopls', 'clangd'}
for _, ls in ipairs(language_servers) do
    require('lspconfig')[ls].setup({
        capabilities = capabilities
        -- you can add other fields for setting up lsp server in this table
    })
end

lspconfig.nil_ls.setup({
  -- on_attach = function(client, _)
  --   client.server_capabilities.hoverProvider = false
  -- end,
  settings = {
    nix = {
      flake = {
        autoArchive = true,
      },
    },
  },
})
lspconfig.nixd.setup({
  cmd = { "nixd", "--inlay-hints=true" },
  on_attach = function(client, bufnr)
    vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
  end,
  settings = {
    nixd = {
      nixpkgs = {
        expr = 'import (builtins.getFlake ("git+file://" + toString ./.)).inputs.nixpkgs { }',
      },
      formatting = {
        command = { "nixpkgs-fmt" },
      },
      options = {
        nixos = {
          expr = '(builtins.getFlake ("git+file://" + toString ./.)).nixosConfigurations.Desktop.options',
        },
        home_manager = {
          expr = '(builtins.getFlake ("git+file://" + toString ./.)).homeConfigurations.work.options',
        },
      },
    },
  },
})

lspconfig.ts_ls.setup {}
lspconfig.gopls.setup {}
lspconfig.lua_ls.setup({
  settings = {
    Lua = {
      completion = {
        callSnippet = "Replace"
      }
    }
  }
})

lspconfig.clangd.setup {}
lspconfig.rust_analyzer.setup {}
lspconfig.zls.setup {}
lspconfig.ols.setup {}
lspconfig.pylsp.setup {}
lspconfig.glslls.setup {}
lspconfig.hls.setup {}

local capabilitiesCss = vim.lsp.protocol.make_client_capabilities()
capabilitiesCss.textDocument.completion.completionItem.snippetSupport = true
lspconfig.cssls.setup {
  capabilities = capabilities,
  cmd = { "css-languageserver", "--stdio" },
}
