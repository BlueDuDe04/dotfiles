local cmp = require 'cmp'

local lspkind = require('lspkind')

local mapping = {
  ['<C-n>'] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
  ['<C-e>'] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },

  ['<PageDown>'] = cmp.mapping.scroll_docs(4),
  ['<PageUp>'] = cmp.mapping.scroll_docs(-4),

  ['<CR>'] = cmp.mapping.confirm {
    behavior = cmp.ConfirmBehavior.Insert,
    select = true,
  },
}

cmp.setup {
  formatting = {
    format = lspkind.cmp_format({
      mode = 'symbol_text',
      maxwidth = 50,

      ellipsis_char = '...',
      show_labelDetails = true,

      -- before = function (entry, vim_item)
      --   return vim_item
      -- end
    })
  },

  mapping = mapping,

  sources = {
    { name = 'nvim_lsp' },
    { name = 'nvim_lua' },
    { name = 'path' },
    { name = "neorg" },
  },
}

-- `/` cmdline setup.
cmp.setup.cmdline('/', {
  mapping = mapping,

  sources = {
    { name = 'buffer' }
  }
})

-- `:` cmdline setup.
cmp.setup.cmdline(':', {
  mapping = mapping,

  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    {
      name = 'cmdline',
      option = {
        ignore_cmds = { 'Man', '!' }
      }
    }
  })
})
