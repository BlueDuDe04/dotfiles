vim.g.mapleader = ' '
vim.g.maplocalleader = ','

require('plugins')
require('keymap')

require("catppuccin").setup({
    flavour = "mocha",              -- latte, frappe, macchiato, mocha
    transparent_background = false, -- disables setting the background color.
    show_end_of_buffer = false,     -- shows the '~' characters after the end of buffers
    term_colors = false,            -- sets terminal colors (e.g. `g:terminal_color_0`)
    dim_inactive = {
        enabled = false,            -- dims the background color of inactive window
        shade = "dark",
        percentage = 0.15,          -- percentage of the shade to apply to the inactive window
    },
    no_italic = false,              -- Force no italic
    no_bold = false,                -- Force no bold
    no_underline = false,           -- Force no underline
    styles = {                      -- Handles the styles of general hi groups (see `:h highlight-args`):
        comments = { "bold" },      -- Change the style of comments
        conditionals = {},
        loops = {},
        functions = { "bold" },
        keywords = { "italic" },
        strings = {},
        variables = {},
        numbers = {},
        booleans = {},
        properties = {},
        types = {},
        operators = {},
        -- miscs = {}, -- Uncomment to turn off hard-coded styles
    },
    color_overrides = {},
    custom_highlights = {},
    default_integrations = true,
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        treesitter = true,
        notify = false,
        mini = {
            enabled = true,
            indentscope_color = "",
        },
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

vim.api.nvim_create_autocmd("VimEnter", {
  callback = function()
    -- Ensure the plugin is loaded before running the command
    vim.schedule(function()
      if vim.fn.argc() == 0 then
        require("oil").open()
      end
    end)
  end
})

if vim.g.neovide then
    vim.g.neovide_fullscreen = false
    vim.g.neovide_remember_window_size = false

    vim.o.guifont = "VictorMono Nerd Font"

    vim.keymap.set('n', '<C-S-s>', ':w<CR>')     -- Save
    vim.keymap.set('v', '<C-S-c>', '"+y')        -- Copy
    vim.keymap.set({ 'n', 'v' }, '<C-S-v>', '"+P') -- Paste
    vim.keymap.set('c', '<C-S-v>', '<C-R>+')     -- Paste command mode
    vim.keymap.set('i', '<C-S-v>', '<C-R>+')     -- Paste insert mode

    vim.g.neovide_scroll_animation_length = 0.05

    vim.g.neovide_padding_top = 10
    vim.g.neovide_padding_bottom = 0
    vim.g.neovide_padding_right = 10
    vim.g.neovide_padding_left = 10

    vim.g.neovide_cursor_animation_length = 0.05
    vim.g.neovide_cursor_trail_size = 0.7
end

require("oil").setup()
vim.keymap.set("n", "<C-[>", "<CMD>Oil<CR>", { desc = "Open parent directory" })

vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.fillchars:append { eob = " " }
vim.opt.conceallevel = 2
vim.opt.ignorecase = true
vim.opt.termguicolors = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

vim.opt.virtualedit = "block"

vim.opt.inccommand = "split"
vim.opt.splitbelow = true
vim.opt.splitright = true

-- vim.opt.foldenable = true
-- vim.opt.foldmethod = 'expr'
vim.opt.foldcolumn = '0'
vim.opt.foldlevel = 99
vim.opt.foldlevelstart = 99
-- vim.opt.foldminlines = 10

-- vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
-- vim.opt.foldtext = ""

vim.opt.wrap = false
vim.opt.scrolloff = 999

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.cache/nvim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.clipboard = "unnamedplus"

vim.cmd.colorscheme "catppuccin"

vim.keymap.set({ "n", "v" }, ";", ":")
vim.keymap.set("n", "<leader>m", "@")

vim.keymap.set("v", "<S-Down>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<S-Up>", ":m '<-2<CR>gv=gv")
