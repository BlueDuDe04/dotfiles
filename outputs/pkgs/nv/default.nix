args @ {
  name,
  inputs,
  ...
}:

let
  pkg = system: let
    pkgs = import inputs.nixpkgs { inherit system; };
  in
    import ./pkg.nix { inherit name inputs system pkgs; };
in

if builtins.hasAttr "system" args then
    pkg args.system
else

let
  systems = [
    # Linux
    "x86_64-linux"
    "aarch64-linux"

    # Macos
    "x86_64-darwin"
    "aarch64-darwin"
  ];

  forAllSystems = inputs.nixpkgs.lib.genAttrs systems;
in

forAllSystems pkg
