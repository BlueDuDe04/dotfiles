{
  name,
  inputs,
  system,
  pkgs,
  lib ? pkgs.lib,
  ...
}:

let
  which-key-nvim = (pkgs.vimUtils.buildVimPlugin { name = "which-key-nvim"; src = inputs.which-key-nvim; });
  trouble-nvim = (pkgs.vimUtils.buildVimPlugin { name = "trouble-nvim"; src = inputs.trouble-nvim; });
  disassemble-nvim = (pkgs.vimUtils.buildVimPlugin { name = "disassemble-nvim"; src = inputs.disassemble-nvim; });

  start = with pkgs.vimPlugins; [
    which-key-nvim
    trouble-nvim
    disassemble-nvim

    # Theme
    catppuccin-nvim

    # Deps
    nvim-web-devicons
    plenary-nvim
    promise-async

    # Treesitter
    # nvim-treesitter.withAllGrammars
    (nvim-treesitter.withPlugins (_: with _; [
      # Langs
      nix
      lua
      zig
      c
      cpp
      glsl
      asm
      python

      # Lint
      luadoc

      # Shells
      bash
      fish

      # Web
      html
      css

      # Configs
      ini
      yaml
      xml
      csv
      toml
      json
      make
      cmake
      hyprlang
      norg

      # Others
      diff
    ]))
    nvim-treesitter-textobjects

    telescope-nvim
    telescope-fzf-native-nvim

    # CMP
    nvim-cmp
    nvim-lspconfig
    otter-nvim
    cmp-buffer
    cmp-cmdline
    cmp-nvim-lsp
    cmp-nvim-lua
    cmp-path
    lspkind-nvim

    # Git
    gitsigns-nvim
    vim-fugitive

    # Neorg
    neorg

    # Neotest
    neotest
    neotest-zig

    # Dap
    # nvim-dap
    # nvim-dap-virtual-text
    # nvim-dap-go

    # Nav
    oil-nvim
    harpoon2

    # UI
    lualine-nvim
    # dashboard-nvim
    nvim-ufo

    # Other
    undotree
    comment-nvim
    nvim-colorizer-lua
    kmonad-vim
    vim-be-good
  ];

  opt = with pkgs.vimPlugins; [];

  deps = with pkgs; [
    ripgrep
    fd

    # LSPs
    lua-language-server
    nil
    nixd

    # Formatters

    # Dap
    # delve # Go
    # lldb # LLVM
  ];

  myNeovim = pkgs.neovim.override {
    configure = {
      packages.myVimPackage = {
        inherit start opt;
      };
    };
  };
in

derivation {
  inherit name;
  system = system;
  builder = "${pkgs.bash}/bin/bash";
  args = [ "-c" /* sh */ ''
    export PATH=${lib.makeBinPath (with pkgs; [
      coreutils
    ])}

    mkdir -p $out/bin

    echo '#! ${pkgs.bash}/bin/bash -e
    export PATH="$PATH:${lib.makeBinPath deps}"
    ${myNeovim}/bin/nvim --cmd "lua package.path=\"${./.}/lua/?.lua;${./.}/lua/?/init.lua;\" .. package.path" -u ${./.}/init.lua "$@"' > $out/bin/nvim

    chmod +x $out/bin/nvim

    echo '#! ${pkgs.bash}/bin/bash -e
    export PATH="$PATH:${lib.makeBinPath deps}"
    ${pkgs.neovide}/bin/neovide --neovim-bin ${myNeovim}/bin/nvim -- --cmd "lua package.path=\"${./.}/lua/?.lua;${./.}/lua/?/init.lua;\" .. package.path" -u ${./.}/init.lua "$@"' > $out/bin/nv

    chmod +x $out/bin/nv
    
    ln -s $out/bin/nvim $out/bin/vi
    ln -s $out/bin/nvim $out/bin/vim
  '' ];
}
