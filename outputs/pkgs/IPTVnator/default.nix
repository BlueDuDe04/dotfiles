let
  systems = [
    "x86_64-linux"
    "aarch64-linux"
    "i686-linux"
  ];

  desktopItem = ''
    [Desktop Entry]
    Categories=Network
    Exec=IPTVnator
    GenericName=Live Tv
    Icon=IPTVnator
    Keywords=live;tv
    Name=IPTVnator
    StartupWMClass=IPTVnator
    Type=Application
    Version=0.15.1
    Comment=Live Tv
  '';
in

args @ {
  name,
  inputs,
  ...
}:

let
  forAllSystems = inputs.nixpkgs.lib.genAttrs systems;

  pkg = system: let
    pkgs = import inputs.nixpkgs { inherit system; };

    iptvnator = pkgs.appimageTools.wrapType2 {
      pname = name;
      version = "v0.15.1";

      src = inputs.iptvnator;
    };
  in with pkgs; stdenv.mkDerivation {
    inherit name;

    src = writeShellScriptBin name ''${iptvnator}/bin/IPTVnator --enable-features=UseOzonePlatform --ozone-platform=wayland "$@"'';

    installPhase = ''
      mkdir -p $out/share/icons/hicolor/256x256/apps
      mkdir -p $out/share/applications

      ln -s $src/bin $out/bin
      echo "${desktopItem}" > $out/share/applications/IPTVnator.desktop
      ln -s ${./icon.png} $out/share/icons/hicolor/256x256/apps/IPTVnator.png
    '';
  };
in


if builtins.hasAttr "system" args then
  pkg args.system
else

forAllSystems pkg
