let
  isDefaultImport = onImport: dir: path: (file: type:
    if type == "directory" then
      if builtins.hasAttr "default.nix" (builtins.readDir (dir + "/${file}")) then
        onImport file (dir + "/${file}") ("/" + path + "/${file}")
      else
        getAll onImport (dir + "/${file}") (path + "/" + builtins.baseNameOf (dir + "/${file}"))
    else null
  );

  getAll = onImport: dir: path: builtins.mapAttrs (
    isDefaultImport onImport dir path
  ) (builtins.readDir dir);

  recImportDirectories = dir: func: (
    if builtins.pathExists dir then 
      getAll (name: file: path: func name file path) dir (builtins.baseNameOf dir)
    else {}
  );

  root = ./outputs;
in

inputs:

recImportDirectories root (name: file: path: import file {
  inherit root recImportDirectories name inputs;
})

//

{
  nixosConfigurations = {
    AI = inputs.self.systems.AI.nixosSystem;
    Desktop = inputs.self.systems.Desktop.nixosSystem;
    Laptop = inputs.self.systems.Laptop.nixosSystem;
    Server = inputs.self.systems.Server.nixosSystem;
    Tv = inputs.self.systems.Tv.nixosSystem;
    Tv-Theater = inputs.self.systems.Tv-Theater.nixosSystem;
    USB = inputs.self.systems.USB.nixosSystem;
    VPN = inputs.self.systems.VPN.nixosSystem;
  };

  topology.x86_64-linux = import inputs.nix-topology {
    pkgs = import inputs.nixpkgs {
      system = "x86_64-linux";

      overlays = [
        inputs.nix-topology.overlays.default
      ];
    };
    modules = [
      { nixosConfigurations = inputs.self.nixosConfigurations; }

      ({ config, ... }: let
        inherit (config.lib.topology) mkInternet mkConnection;
      in {
        nodes.internet = mkInternet {
          connections = [
            (mkConnection "Server" "enp2s0")
            (mkConnection "VPN" "enp1s0")
          ];
        };
      })
    ];
  };
}
